Starting weekly assessment for Hanyun, Week6

Current Points = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
Your Git repo size this week is about 5.93 MiB on disk 

PART 1: Checking project workflow...

Found the following directories in parent directory: Week1, Week7, Assessment, Week2, Mainproject, .git, Miniproject, Week3

Found the following files in parent directory: .gitignore, README.md

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~

# Temporary files
*.tmp
*.swp
*.p

# VSC
*.vscode

# Python
*.py[cod]
__pycache__/

# R History files
*.Rhistory
*.Rapp.history

# R Session Data files
*.RData
*.tar.gz

# RStudio files
*.Rproj.user/

# Latex auxiliary files
*.aux
*.lof
*.log
*.lot
*.fls
*.out
*.toc
*.fmt
*.fot
*.cb
*.cb2
.*.lb

# Bibliography auxiliary files
*.bbl
*.bcf
*.blg
*-blx.aux
*-blx.bib
*.run.xml
**********************************************************************

Found README in parent directory, named: README.md

Printing contents of README.md:
**********************************************************************
# == CMEE Coursework Repository 2018-2019 ==
![Alt text](http://news.mit.edu/sites/mit.edu.newsoffice/files/images/2015/MIT-Data-Sci-Machine_0.jpg)
# Overview
  Include scripts and data for [CMEE coursework](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/Index.ipynb), sorting by weeks.  
  Author: Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)  
  Date: 2018-2019  
  Input files are in sandbox or data directory.Output files are saved to results.  
  Please find detailed description in weekly README.txt.

# Contents
## [Week1](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week1/)
####[Introduction to Unix](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/01-Unix.ipynb)
####[Shell scripting](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/02-ShellScripting.ipynb)
  * Command arguments
  * Redirection and pipes
  * Wildcards
  * Using grep
  * Finding files
####[Version Control with Git](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/03-Git.ipynb)
####[Scientific documents with LATEX](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/04-LaTeX.ipynb)

## [Week2](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week2/)
####[Biological Computing in Python-I](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/05-Python_I.ipynb)
  * Python basics
  * Python Input/Output
  * Python programs
  * Control statements
  * Debugging

## [Week3](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week3/)
####[Biological Computing in R](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/07-R.ipynb)
  * R basics
  * Vectorization
  * Debugging
  * Sweave and knitr
  * Data wrangling
  * Data visualization
  * qplot and ggplot
 
## [Week7](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week7/)
####[Biological Computing in Python-II](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/06-Python_II.ipynb)
  * Manipulation of scipy arrays
  * Plotting with matplotlib
  * Profiling with run -p and timeit
  * Networks
  * Regular expressions
  * Build workflows

# Dependencies
 
### Ubuntu 16.04
 
### Python 3.6.3
####Modules
  * pickle
  * numpy
  * scipy
  * pandas
  * networkx
  * matplotlib
 
### R 3.5.1
####Packages
  * lattice
  * reshape2
  * dplyr
  * tidyr
  * ggplot2
  * maps
  * igraph
  * knitr

### LaTeX



     


**********************************************************************

======================================================================
Looking for the weekly directories...

Found 4 weekly directories: Week1, Week2, Week3, Week7

The Week6 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Points for the Week = 100

NOTE THAT THESE ARE POINTS, NOT MARKS FOR THE WEEK!