=== MY CMEE Coursework Directory - Week 1 ===

Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
Date: Oct 2018

== Description ==

Include the script, data and sandbox files for the 1st week practical.

# Input files are in Sandbox or Data directory #
# Output files are saved to the Result directory#
# Please follow the description at the beginning of each script to choose arguments #

== List of files ==

├── Code
│   ├── boilerplate.sh
│   ├── ConcatenateTwoFiles.sh
│   ├── CountLines.sh
│   ├── csvtospace.sh
│   ├── Latex
│   │   ├── CompileLaTeX.sh
│   │   ├── FirstBiblio.bib
│   │   └── FirstExample.tex
│   ├── MyExampleScript.sh
│   ├── tabtocsv.sh
│   ├── tiff2png.sh
│   ├── UnixPrac1.txt
│   └── variables.sh
├── Data
│   ├── 407228326.fasta
│   ├── 407228412.fasta
│   ├── E.coli.fasta
│   ├── spawannxs.txt
│   └── Temperatures
│       ├── 1800.csv
│       ├── 1801.csv
│       ├── 1802.csv
│       └── 1803.csv
├── README.txt
├── Result
└── Sandbox


12 directories, 35 files

== Scripts description == 

# boilerplate.sh #	
Desc: simple boilerplate for shell scripts
Arguments: none

# ConcatenateTwoFiles.sh #
Desc: merge the content of two files into one
Arguments: 1-> ../Sandbox/Test1.txt 2-> ../Sandbox/Test2.txt 3-> ../Sandbox/Test3.txt

# CountLines.sh #
Desc: count the lines of the input file
Arguments: 1-> ../Sandbox/ListRootDir.txt

# MyExampleScript.sh #
Desc: simple example of the use of variables
Auguments: none

# tabtocsv.sh #
Desc: substitute the tabs in the files with commas
Arguments: 1-> ../Sandbox/test.txt

# variables.sh #
Desc: examples of the use of variables, replace the value of variable with input string, compute the sum of two input integers
Arguments: string, two integers seperated by space 

# tiff2png.sh #
Desc: convert tiff to png
Arguments: none #the tiff file is in ../Sandbox

# csvtospace.sh #
Desc: substitute the commas in the files with space
Arguments: ../Data/Temperatures/1800.csv ../Data/Temperatures/1801.csv ../Data/Temperatures/1802.csv ../Data/Temperatures/1803.csv

# UnixPrac1.txt #
Desc: 
#1 Count how many lines are in each file
#2 Print everything starting from the second line for the E. coli genome
#3 Count the sequence length of this genome
#4 Count the matches of a particular sequence, "ATGC" in the genome of E. coli
#5 Compute the AT/GC ratio
Arguments: none #all .fasta files are in ../Data

# CompileLaTeX.sh #
Desc: compile latex with bibtex and create the pdf file
Arguments: 1-> FirstExample.tex

