#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
#Des:Convert the .tiff files in Sandbox to .jpg files.
#Argument: none #.tiff file in ../Sandbox
#save the output to ../Result
# Date: Oct 2018

for f in ../Sandbox/*.tiff; 
    do  
        echo "Converting $f"; 
        convert "$f"  "../Result/$(basename "$f" .tiff).jpg"; 
        echo "Done!"
    done
