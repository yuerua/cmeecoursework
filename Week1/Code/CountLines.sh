#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Script: CountLines.sh
# Desc: count the lines in input file
# Arguments: ../Sandbox/ListRootDir.txt
# Date: Oct 2018

# Check if input file is provided
if [ -z "$1" ];
	then
	echo -e "\nPlease supply the file\n"
	exit 1

else
NumLines=`wc -l < $1`
echo "The file $1 has $NumLines lines"
fi
