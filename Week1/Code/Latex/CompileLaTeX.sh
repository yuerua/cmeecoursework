#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
#Desc: compile latex with bibtex and create the pdf file
#save the output as ../../Result/FirstExample.pdf
#Arguments: 1-> FirstExample.tex
# Date: Oct 2018

# Check if input file is provided
if [ -z "$1" ];
	then
	echo -e "\nPlease supply the file\n"
	exit 1

else
basename=`basename $1 .tex`
pdflatex  $1
bibtex $basename
pdflatex $1 
pdflatex $1
mv ${basename}.pdf ../../Result/
evince ../../Result/${basename}.pdf
## Cleanup
rm *.aux
rm *.log
rm *.bbl
rm *.blg
fi









