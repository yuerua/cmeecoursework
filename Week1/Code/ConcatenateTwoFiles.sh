#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Script: ConcatenateTwoFiles.sh
# Desc: merge two input files into one, save the output text in the third input file
# Arguments: ../Sandbox/Test1.txt ../Sandbox/Test2.txt ../Sandbox/Test3.txt
# Date: Oct 2018

# Check if input files are provided
if [ $# -lt 3 ]; # If arguments are less than three
	then
	echo -e "\nPlease supply three files and try again\n"
	exit 1

else
# Write file1 to file3
cat $1 > $3
# Append file2 to file3
cat $2 >> $3
echo "Merged File is"
# Print file3
cat $3
fi
