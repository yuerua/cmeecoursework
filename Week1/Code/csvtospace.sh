#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Script: csvtospace.sh
# Desc: substitute the commas in the files with space
# saves the output into a .txt file, in ../Result
# Arguments: ../Data/Temperatures/1800.csv ../Data/Temperatures/1801.csv ../Data/Temperatures/1802.csv ../Data/Temperatures/1803.csv
# Date: Oct 2018

# Check if input file is provided
if [ -z "$1" ];
	then
	echo -e "\nPlease supply the file to be converted\n"
	exit 1

else
echo "Creating a space delimited version of $1 ..."
cat $1 | tr "," " " >> $1.ssv
mv $1.ssv ../Result 
echo "Done!"
fi
