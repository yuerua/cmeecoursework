#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Script: tabtocsv.sh
# Desc: substitute the tabs in the files with commas
# saves the output into a .csv file, in ../Result 
# Arguments: 1-> ../Sandbox/test.txt
# Date: Oct 2018

# Check if input file is provided
if [ -z "$1" ];
	then
	echo -e "\nPlease supply the file to be converted\n"
	exit 1

else
echo "Creating a comma delimited version of $1 ..."
cat $1 | tr -s "\t" "," >> $1.csv
mv $1.csv ../Result
echo "Done!"
fi
