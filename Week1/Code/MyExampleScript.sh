#!/bin/bash
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Script: MyExampleScript.sh
# Desc: simple example of the use of variables
# Auguments: none
# Date: Oct 2018

msg1="Hello"
msg2=$USER
echo "$msg1 $msg2"
echo "Hello $USER"
echo
