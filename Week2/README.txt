=== MY CMEE Coursework Directory - Week 2 ===

Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
Date: Oct 2018

== Description ==

Include the script, data and sandbox files for the 1st week practical.

# Input files are in sandbox or Data directory #
# Output files are saved to Result directory #

== List of files ==               == Description ==

├── Code
│   ├── align_seqs_better.py      # Align two sequences to find the best alignment, and save all alignments with equal best score
│   ├── align_seqs_fasta.py       # Align two sequences from input fasta files or use default sequences to find the best alignment
│   ├── align_seqs.py             # Align two sequences from input or use default sequences to find the best alignment
│   ├── basic_csv.py              # Functions showing how to deal with csv file with the csv module
│   ├── basic_io.py               # Simple boilerplate for file input and output, and the use of pickle for storing objects
│   ├── boilerplate.py            # Boilerplate of a python program
│   ├── cfexercises1.py           # Some functions clarifying the use of range()
│   ├── cfexercises2.py           # Some functions exemplifying the use of control statements
│   ├── control_flow.py           # Some functions exemplifying the use of control statements
│   ├── debugme.py                # Boilerplate for debugging using %pdb
│   ├── dictionary.py             # Populate a dictionary to map order names to sets of taxa
│   ├── lc1.py                    # Use list comprehension and loops to deal with information
│   ├── lc2.py                    # Use list comprehension and loops to deal with information
│   ├── loops.py                  # Examples of the use of loops
│   ├── oaks_debugme.py           # Finds only oak trees taxas from a list of species, and save all the oak trees to a csv file
│   ├── oaks.py                   # Finds just those taxa that are oak trees from a list of species
│   ├── scope.py                  # Functions to interpretate the global and local variables
│   ├── sysargv.py                # Boilerplate to clarify sys.argv
│   ├── test_control_flow.py      # Some examples exemplifying the use of doctest
│   ├── tuple.py                  # Loops through each tuple in tuple "birds", print elements of each tuple on separate lines
│   └── using_name.py             # Boilerplate to interpretate the setting of module name
├── Data
│   ├── 407228326.fasta
│   ├── 407228412.fasta
│   ├── DNA.csv
│   ├── E.coli.fasta
│   ├── testcsv.csv
│   └── TestOaksData.csv
├── README.txt
├── Result
└── sandbox

4 directories, 31 files
