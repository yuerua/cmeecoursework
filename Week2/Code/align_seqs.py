#!/usr/bin/env python3

"""Align two sequences from input or use default sequences 
    to find the best alignment"""

__appname__ = 'align_seqs.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import sys
import csv

# Default sequence without input
if len(sys.argv) == 1:
    seq2 = "ATCGCCGGATTACGGG"
    seq1 = "CAATTCGGAT"
else:
    # Input sequence from .csv file
    # e.g ../Data/DNA.csv
    f = open(sys.argv[1],'r')
    file = csv.reader(f)
    seq = []
    for line in file:
        seq.append(line)
    # save each line as a sequence 
    seq1 = "".join(seq[0])
    seq2 = "".join(seq[1])
    f.close()

# Assign the longer sequence s1, and the shorter to s2
# l1 is length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths

# Functions
def calculate_score(s1, s2, l1, l2, startpoint):
    """ computes a score by returning the matches starting from arbitrary startpoint """
    matched = "" # to hold string displaying alignements
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            if s1[i + startpoint] == s2[i]: # if the bases match
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"
    # some formatted output
    print("." * startpoint + matched)           
    print("." * startpoint + s2)
    print(s1)
    print(score) 
    print(" ")
    
    return score
    

# Test the function with some example starting points:
# calculate_score(s1, s2, l1, l2, 0)
# calculate_score(s1, s2, l1, l2, 1)
# calculate_score(s1, s2, l1, l2, 5)

# find the best match (highest score) for the two sequences
my_best_align = None
my_best_score = -1

for i in range(l1): 
    # calculate scores for each startpoint
    z = calculate_score(s1, s2, l1, l2, i)
    # replace current best score and alignment if the next score is higher
    if z > my_best_score:
        my_best_align = "." * i + s2 
        my_best_score = z 
# save the best alignment and score to DNA_output.txt
output = open('../Result/DNA_output.txt','w')        
output.write(my_best_align + '\n' + s1 + '\n' + "Best score:" + str(my_best_score))

# closes the files
output.close()

# Print results
with open('../Result/DNA_output.txt') as result:
    print(result.read())
