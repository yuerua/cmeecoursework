#!/usr/bin/env python3

"""Align two sequences to find the best alignment,
and save all alignments with equal best score"""

__appname__ = 'align_seqs_better.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import pickle

# These are the two sequences to match
seq2 = "ATTAACGCCGGATAGGCTAGGATCTGAAACGCCGGATTACGGG"
seq1 = "CAATTCGGAT"

# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths

def calculate_score(s1, s2, l1, l2, startpoint):
    """ computes a score by returning the matches starting from arbitrary startpoint """
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    # some formatted output
    print("." * startpoint + matched)           
    print("." * startpoint + s2)
    print(s1)
    print(score) 
    print(" ")

    return score

my_best_align = None
my_best_score = -1
best_align_num = 0 # count the best alignment numbers
f = open('../Result/best_alignment.p','wb') # To store all best alignments

# find the highest score 
for i in range(l1):
    # calculate scores for each startpoint
    z = calculate_score(s1, s2, l1, l2, i)
    # replace current best score if the next score is higher
    if z > my_best_score:
        my_best_score = z

# find all alignments with scores equal to the highest one
# save to a file
for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z == my_best_score:
        my_best_align = "." * i + s2
        best_align_num += 1
        pickle.dump(my_best_align, f)
f.close()

f = open('../Result/best_alignment.p','rb')
best_align = open('../Result/best_alignment_better.txt','w')

for i in range(best_align_num):
    best_align.write(pickle.load(f)+'\n'+s1+'\n'+"Best score:"+str(my_best_score)+'\n')

# closes files
f.close()
best_align.close()

# Print results
with open('../Result/best_alignment_better.txt') as output:
    print(output.read())

