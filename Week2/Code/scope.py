#!/usr/bin/env python3

"""Functions to interpretate the global and local variables"""

__appname__ = 'scope.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

## Try this first

_a_global = 10

def a_function():
    """ assign local variables """
    _a_global = 5
    _a_local = 4
    print("Inside the function, the value is ", _a_global)
    print("Inside the function, the value is ", _a_local)
    return None

a_function()

print("Outside the function, the value is ", _a_global)


## Now try this

_a_global = 10

def a_function():
    """ assign global variables """
    global _a_global
    _a_global = 5
    _a_local = 4
    print("Inside the function, the value is ", _a_global)
    print("Inside the function, the value is ", _a_local)
    return None

a_function()
print("Outside the function, the value is", _a_global)