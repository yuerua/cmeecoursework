#!/usr/bin/env python3

"""Some functions exemplifying the use of control statements"""
__appname__ = 'cfexercises2.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import sys

# Functions
def foo1(x=9):
    """ calculate the square root of x """
    return x ** 0.5

def foo2(x=3, y=5):
    """ return the bigger number of x,y """
    if x > y:
        return x
    return y

def foo3(x=7, y=5, z=3):
    """ swap two numbers if the one on the left is bigger """
    if x > y:
        tmp = y
        y = x
        x = tmp
    if y > z:
        tmp = z
        z = y
        y = tmp
    return [x, y, z]

def foo4(x=3):
    """ calculate 1*2*3...*x """
    result = 1
    for i in range(1, x + 1):
        result = result * i
    return result

def foo5(x=4): # a recursive function
    """ calculate x*(x-1)*(x-2)...*1 """
    if x == 1:
        return 1
    return x * foo5(x - 1)

def main(argv):
    """ Test functions with some example inputs """
    print(foo1())
    print(foo2())
    print(foo3())
    print(foo4())
    print(foo5())
    return 0

if (__name__ == "__main__"):
    """ Makes sure the "main" function is called from command line """  
    status = main(sys.argv)
    sys.exit(status)