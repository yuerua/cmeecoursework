#!/usr/bin/env python3

"""Use list comprehension or loops to gather latin names, common names 
and body mass of birds in separate lists """

__appname__ = 'lc1.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

#Data
birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 
latin_name = list(species[0] for species in birds)
common_name = list(species[1] for species in birds)
body_mass = list(species[2] for species in birds)
print(latin_name)
print(common_name)
print(body_mass)

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 
latin_name = []
common_name = []
body_mass = []
for species in birds:
    latin_name.append(species[0])   #Add latin names of each specie into list latin_name
    common_name.append(species[1]) #Add common names of each specie into list common_name
    body_mass.append(species[2])     #Add body mass of each specie into list body_mass
print(latin_name)
print(common_name)
print(body_mass)

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS.
