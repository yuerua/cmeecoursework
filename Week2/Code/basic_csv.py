#!/usr/bin/env python3

"""Functions showing how to deal with csv file with the csv module"""

__appname__ = 'basic_csv.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@email.address)'
__version__ = '0.0.1'

# Imports
import csv

# Read a file containing:
# 'Species','Infraorder','Family','Distribution','Body mass male (Kg)'
f = open('../Data/testcsv.csv','r')

csvread = csv.reader(f)
temp = []
for row in csvread:
    temp.append(tuple(row))
    print(row)
    print("The species is", row[0])

f.close()

# write a file containing only species name and Body mass
f = open('../Data/testcsv.csv','r')
g = open('../Result/bodymass.csv','w')

csvread = csv.reader(f)
csvwrite = csv.writer(g)
for row in csvread:
    print(row)
    csvwrite.writerow([row[0], row[4]])
# closes files
f.close()
g.close()
