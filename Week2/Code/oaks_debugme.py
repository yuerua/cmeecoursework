#!/usr/bin/env python3

"""Finds only oak trees taxas from a list of species, 
and save all the oak trees to a csv file"""

__appname__ = 'oaks_debugme.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import csv
import sys
import doctest

#Define function
def is_an_oak(name):
    """ Find species with genus name 'quercus' 
    
    >>> is_an_oak('Quercus robur')
    True
    
    >>> is_an_oak('Fagus sylvatica')
    False
    
    >>> is_an_oak('Quercuss robur')
    False"""
    #Return taxas with genus name strictly 'quercus'
    return name.lower().split()[0] == 'quercus'


def main(argv): 
    """Find just oak trees and save to a file"""
    
    f = open('../Data/TestOaksData.csv','r')
    g = open('../Result/JustOaksData.csv','w')
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    
    # Excludes the header row in input file
    next(taxa)
    # Add header row to output file
    csvwrite.writerow(['Genus', ' species'])
    
    for row in taxa:
        print(row)
        print ("The genus is: ") 
        print(row[0] + '\n')
        if is_an_oak(row[0]):
            print('FOUND AN OAK!\n')
            csvwrite.writerow([row[0], row[1]])    
    # closes files
    f.close()
    g.close()
    return 0

if (__name__ == "__main__"):
    status = main(sys.argv)

doctest.testmod() # To run with embedded tests
