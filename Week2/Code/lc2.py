#!/usr/bin/env python3

"""Use list comprehension and loops to deal with information"""

__appname__ = 'lc2.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Data
# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# # the amount of rain was greater than 100 mm.

rain_list = list(i for i in rainfall if i[1] > 100)
print (rain_list) 

# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

month_list = list(i[0] for i in rainfall if i[1] < 50)
print (month_list)

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 
# Create two empty list
rain_list = []
month_list = []
for i in rainfall:
    # Add tuples with rainfall greater than 100 to rain_list
    if i[1] > 100: 
        rain_list.append(i)
    # Add the months with rainfall less than 50 to month_list
    if i[1] < 50:
        month_list.append(i[0])
print (rain_list)
print (month_list)

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
