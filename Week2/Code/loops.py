#!/usr/bin/env python3

"""Examples of the use of loops"""
__appname__ = 'loops.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# FOR loops in Python
for i in range(5):
    # print integers from 0 to 4
    print(i)

my_list = [0, 2, "geronimo!", 3.0, True, False]
for k in my_list:
    # print each object in list
    print(k)

total = 0
summands = [0, 1, 11, 111, 1111]
for s in summands:
    # add up numbers in list one by one, and print the sum
    total = total + s
    print(total)

# WHILE loops  in Python
z = 0
while z < 100:
    # print integers from 1 to 100
    z = z + 1
    print(z)

b = True
while b:
    # print the string infinitely
    print("GERONIMO! infinite loop! ctrl+c to stop!")
# ctrl + c to stop!