#!/usr/bin/env python3

"""Align two sequences from input fasta files or use default 
sequences to find the best alignment"""

__appname__ = 'align_seqs_fasta.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import sys
import csv

# Default sequence without input
if len(sys.argv) == 1:
    file1 = open('../Data/407228326.fasta','r')
    file2 = open('../Data/407228412.fasta','r')
# Input sequence from .fasta file
else:
    file1 = open(sys.argv[1],'r')
    file2 = open(sys.argv[2],'r')

# Excludes the head line
next(file1)
next(file2)

# Removes the newline characters
seq1 = ""
seq2 = ""
for line1 in file1:
    seq1 += line1
for line2 in file2:
    seq2 += line2
seq1 = seq1.replace('\n','')
seq2 = seq2.replace('\n','')

# swap the two lengths
# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest
l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1


def calculate_score(s1, s2, l1, l2, startpoint):
    """ computes a score by returning the matches starting from arbitrary startpoint """
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"
    return score

# find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    # calculate scores of each startpoint
    z = calculate_score(s1, s2, l1, l2, i)
    # replace current best score if the next score is higher
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

# save the best align and score to file ../Result/fasta_output.txt
f = open('../Result/fasta_output.txt','w')
f.write(my_best_align+'\n'+s1+'\n'+str(my_best_score))

# closes the files
file1.close()
file2.close()
f.close()

# Print results
with open('../Result/fasta_output.txt') as output:
    print(output.read())