#!/usr/bin/env python3

"""Boilerplate of a python program"""

__appname__ = 'boilerplate.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@email.address)'
__version__ = '0.0.1'
__license__ = "License for this code/program"

## imports ##
import sys # module to interface our program with the operating system

## constants ##


## functions ##
def main(argv):
    """ Main entry point of the program """
    print('This is a boilerplate') # NOTE: indented using two tabs or 4 spaces
    # return 0 when main runs successfully
    return 0

if __name__ == "__main__": 
    """ Makes sure the "main" function is called from command line """  
    status = main(sys.argv)
    # status = 0 indicates successful termination
    sys.exit(status)