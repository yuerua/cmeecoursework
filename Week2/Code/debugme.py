#!/usr/bin/env python3

"""Boilerplate for debugging using %pdb"""

__appname__ = 'debugme.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

def createabug(x):
    """ Create an error """
    y = x**4
    z = 0.
    y = y/z
    return y

createabug(25)