#!/usr/bin/env python3

"""Boilerplate to interpretate the setting of module name"""

__appname__ = 'using_name.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# If the module is opened as the main module, its name
# is set as default 'main'. If the module is imported to other modules, its
# name will be changed.
if __name__ == '__main__':
    print('This program is being run by itself')
else:
    print('I am being imported from another module')