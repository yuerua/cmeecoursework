#!/usr/bin/env python3

"""Some functions clarifying the use of range()"""

__appname__ = 'cfexercises1.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

for i in range(3, 17):
    print('hello')

for j in range(12):
    if j % 3 == 0: # if j is divisible by 3
        print('hello')

for j in range(15):
     if j % 5 == 3:# if the remainder is 3
        print('hello')
     elif j % 4 == 3:
        print('hello')

z = 0
while z != 15: # while z is not equal to 15
    print('hello')
    z = z + 3

z = 12
while z < 100:
    if z == 31:
        for k in range(7): # print 7 times
            print('hello')
    elif z == 18:
        print('hello')
    z = z + 1