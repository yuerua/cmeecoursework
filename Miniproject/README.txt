=== MY CMEE Coursework Directory - Miniproject ===

Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
Date: Mar 2019

== Description ==

Include the script, data and sandbox files for the CMEE Miniproject

To run the workflow, bash Miniproject.sh

# Input files are in data directory #
# Output files are saved to results directory #

== List of files ==               == Description ==

.
├── code
│   ├── Miniproject.sh            Bash script to run the workflow and compile the report
│   ├── NLLS.py                   Python script to perform the model fitting with NLLS method
│   ├── plot.R                    Basic plot and tables for model fitting results
│   ├── plot_type.R               Analysis of model selection with different criterion
│   ├── References.bib            Reference for report
│   ├── report.tex                Write up the report 
│   └── sortdata.R                R script to sort the original dataset
├── data
│   └── BioTraits.csv             Original database
├── README.txt
├── results
└── sandbox


== Dependencies ==
 
# Ubuntu 16.04
 
# Python 3.6.3
# Modules
  * pandas
  * numpy
  * lmfit
 
# R 3.5.1
# Packages
  * ggplot2
  * reshape2
  * plyr
  * dplyr
  * gridExtra

# LaTeX
