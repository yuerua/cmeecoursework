#!/usr/bin/env python3

"""Perform model fitting with NLLS method"""
__appname__ = 'NLLS.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

#Imports
import sys
import pandas as pd
import numpy as np
from lmfit import minimize, Parameters
from datetime import datetime

#Constant
k = 8.617e-5
e = np.exp(1)

#Set seed
np.random.seed(36)

#Import data and select columns to be used
data = pd.read_csv("../data/BioTraits_modified.csv", usecols =["id","ConTemp","ConTemp_F", 
      "OriginalTraitValue", "OriginalTraitValue_log","B0","E","Eh","El","Th","Tl"])

#Cubic model
def Cubic_model(id, data):
    """ Perform NLLS model fitting on TPC with cubic model """
    #Variabes
    T = np.asarray(data.ConTemp[data.id == id]) #Temperature
    B = np.asarray(data.OriginalTraitValue[data.id == id]) #Trait value
    #Add parameters
    p = Parameters()
    #Starting values of coefficient are set to 0 since they don't have biological meaning
    p.add_many(("B0",0),("B1",0),("B2",0),("B3",0))
    
    #Calculate and minimize the residules
    def Cubic_residual(p, T, B):
        """ Return residuals of data and model """
        B0 = p["B0"].value
        B1 = p["B1"].value
        B2 = p["B2"].value
        B3 = p["B3"].value

        model = B0+ B1*T + B2*T**2 + B3*T**3

        return model - B
    
    try:
        #Try fitting
        output = minimize(Cubic_residual, p ,args = (T,B)) 
        #Results for model comparison
        RSS = sum(Cubic_residual(output.params, T, B)**2) #Residual sum of squares
        TSS = sum((B - np.mean(B))**2)                    #Total sum of squares
        Rsq = 1 - (RSS/TSS)                               #R-squared value
        aicc = output.aic + 2*4*((4+1)/(len(T) - 4 - 1))  #Calculate AICc from AIC value
    
        all = {"id":[id], 
            "B0":output.params["B0"].value,
            "B1":output.params["B1"].value,
            "B2":output.params["B2"].value,
            "B3":output.params["B3"].value,
            "chisqr": [output.chisqr],
            "RSS": [RSS],
            "TSS": [TSS],
            "Rsq": [Rsq],
            "aic":[output.aic],
            "bic":[output.bic],
            "aicc": [aicc]
            }
    
    except ValueError:  #If didn't converge, return an empty data frame
        all = {"id":[id], 
            "B0":[B0],
            "B1":[B1],
            "B2":[B2],
            "B3":[B3],
            "chisqr": [np.NAN],
            "RSS": [np.NAN],
            "TSS": [np.NAN],
            "Rsq": [np.NAN],
            "aic":[np.NAN],
            "bic":[np.NAN],
            "aicc":[np.NAN]
            } 

    return pd.DataFrame(all)

#Briere model

def Briere_model(id, data, min_round, max_round):
    """ Perform NLLS model fitting on TPC with Briere model"""
    #Variabes
    T = np.asarray(data.ConTemp[data.id == id]) #Temperature
    B = np.asarray(data.OriginalTraitValue[data.id == id]) #Trait value
    #Starting values
    B0 = 0.01
    T0 = min(T)
    Tm = max(T)

    #Calculate and minimize the residules

    def Briere_residual(p, T, B):
        """ Residuals of data and models """
        B0 = p["B0"].value
        T0 = p["T0"].value 
        Tm = p["Tm"].value

        model = B0 * T * (T-T0) * (Tm - T)**0.5

        return model - B
    
    #Initiate an empty dictionary for results
    all = {"id":[id], 
            "B0":[B0],
            "T0":[T0],
            "Tm":[Tm],
            "chisqr": [np.NAN],
            "RSS": [np.NAN],
            "TSS": [np.NAN],
            "Rsq": [np.NAN],
            "aic":[np.NAN],
            "bic":[np.NAN],
            "aicc": [np.NAN],
            }
    
    round = 0
    while round < max_round:
        
        round += 1 #Try round +1
        #If TPC converged within mean rounds, or max round is exceeded, break the loop
        if all["aic"] != [np.NaN] and round > min_round: 
            break

        #Starting values defined for rounds
        #For the first round use setted starting values
        if round == 1 : 
            p = Parameters()
            p.add_many(("B0",B0),("T0",T0),("Tm",Tm))
        #For other rounds, assign B0 with random number distributed between 0 and B0*2
        else:
            p = Parameters() 
            p.add("B0", value = np.random.uniform(0,1), min = 0)
            p.add("T0", value = T0, min = -30, max = 130)
            p.add("Tm", value = Tm, min = -30, max = 130)

        try:   
            output = minimize(Briere_residual, p, args = (T,B))
            RSS = sum(Briere_residual(output.params, T, B)**2)
            TSS = sum((B - np.mean(B))**2)
            Rsq = 1 - (RSS/TSS)
            aicc = output.aic + 2*3*((3+1)/(len(T) - 3 - 1))
            #Replace the fitting results if aicc is smaller, 
            # or if the previous fit didn't converge
            if aicc < all["aicc"] or all["aicc"] == [np.NaN]:
                all = {"id":[id], 
                    "B0":[output.params["B0"].value],
                    "T0":[output.params["T0"].value],
                    "Tm":[output.params["Tm"].value],
                    "chisqr": [output.chisqr],
                    "RSS": RSS,
                    "TSS": TSS,
                    "Rsq": Rsq,
                    "aic":[output.aic],
                    "bic":[output.bic],
                    "aicc": aicc
                    }
    
        except ValueError:
            pass
        
        continue

    return pd.DataFrame(all)

#Schoolfield model
#It turns out that the model converge more TPCs when fits on original scale, 
#but for some of the TPCs, parameters estimated from log scale make more sense...,
#so fit with log transformed data
def Schoolfield_model(id, data, min_round, max_round):
    """ Perform NLLS model fitting on logged trait value with schoolfield model """
    #Variabes
    T = np.asarray(data.ConTemp_F[data.id == id]) #Temperature in kelvin
    B = np.asarray(data.OriginalTraitValue[data.id == id]) #Unlogged Trait value
    B_log = np.asarray(data.OriginalTraitValue_log[data.id == id]) #Logged Trait value
    #Starting values
    B0 = data.B0[data.id == id].iloc[0]
    E = abs(data.E[data.id == id].iloc[0])
    Tl = data.Tl[data.id == id].iloc[0]
    Th = data.Th[data.id == id].iloc[0]
    El = abs(data.El[data.id == id].iloc[0])
    Eh = abs(data.Eh[data.id == id].iloc[0])

    def Schoolfield_residual(p, T, B):
        """ Return residuals for unlogged trait values schoolfield model"""

        B0 = p['B0'].value
        E  = p['E'].value
        Tl = p['Tl'].value
        Th = p['Th'].value
        El = p['El'].value
        Eh = p['Eh'].value

        model = (B0*e**((-E/k)*((1/T)-(1/283.15))))/(
                1+(e**((El/k)*((1/Tl)-(1/T))))+(e**((Eh/k)*((1/Th)-(1/T)))))

        return model - B
    
    def Schoolfield_residual_log(p, T, B_log):
        """ Return residuals for logged trait values and logged schoolfield model"""

        B0 = p['B0'].value
        E  = p['E'].value
        Tl = p['Tl'].value
        Th = p['Th'].value
        El = p['El'].value
        Eh = p['Eh'].value

        model = (B0*e**((-E/k)*((1/T)-(1/283.15))))/(
            1+(e**((El/k)*((1/Tl)-(1/T))))+(e**((Eh/k)*((1/Th)-(1/T)))))

        return np.log(model) - B_log

    #Initiate an empty dictionary for results
    all = {"id":[id], 
            "B0":[B0],
            "E":[E],
            "Tl":[Tl],
            "Th":[Th],
            "El": [El],
            "Eh": [Eh],
            "chisqr":[np.NAN],
            "RSS": [np.NAN],
            "TSS": [np.NAN],
            "Rsq": [np.NAN],
            "aic":[np.NAN],
            "bic":[np.NAN],
            "aicc":[np.NAN],
            "RSS_log": [np.NAN],
            "TSS_log": [np.NAN],
            "Rsq_log": [np.NAN],
            "aic_log": [np.NAN],
            "bic_log": [np.NAN],
            "aicc_log": [np.NAN]
            }
    
    round = 0
    while round < max_round:
    
        round += 1 #Try round +1
        #If TPC converged within mean rounds, or max round is exceeded, break the loop
        if all["aic"] != [np.NaN] and round > min_round: 
            break

        #Starting values assigned for rounds
        #For the first round use set starting values
        if round == 1 : 
            #Add starting values
            p = Parameters()
            p.add("B0", value = B0, min = 0)
            p.add("E", value = E, min = 0)
            p.add("Tl",Tl, min = 250, max = 400)
            p.add("Th",Th, min = 250, max = 400)
            p.add("El",El, min = 0)
            p.add("Eh",Eh, min = 0)
        #For other rounds, assign B0 with random number distributed between 0 and B0*2
        else:
            p = Parameters() 
            p.add("B0", value = np.random.uniform(0,B0*2), min = 0)
            p.add("E", value = np.random.uniform(0,E*2), min = 0)
            p.add("Tl",Tl, min = 250, max = 400)
            p.add("Th",Th, min = 250, max = 400)
            p.add("El", value = np.random.uniform(0, El*2),min = 0)
            p.add("Eh", value = np.random.uniform(0, Eh*2),min = 0)
    
    #Results for model fitting on logged values
        try:    
            output = minimize(Schoolfield_residual_log, p, args = (T, B_log))
            RSS_log = sum(Schoolfield_residual_log(output.params,T,B_log)**2)
            TSS_log = sum((B_log - np.mean(B_log))**2)
            Rsq_log = 1 - (RSS_log/TSS_log)
            aicc_log = output.aic + 2*6*((6+1)/(len(T) - 6 - 1))
            #Calculate results for unlogged fitting based on parameters estimated from 
            #logged fitting, since they stay the same after log transformation.
            RSS = sum(Schoolfield_residual(output.params,T,B)**2)
            TSS = sum((B - np.mean(B))**2)
            Rsq = 1 - (RSS/TSS)
            aic = len(T)*np.log(RSS/len(T)) + 2*6
            bic = len(T)*np.log(RSS/len(T)) + np.log(len(T))*6
            aicc = aic + 2*6*((6+1)/(len(T) - 6 - 1))

            if aicc < all["aicc"] or all["aicc"] == [np.NaN]:           
                all = {"id":[id], 
                    "B0":[output.params["B0"].value],
                    "E":[output.params["E"].value], 
                    "Tl":[output.params["Tl"].value],
                    "Th":[output.params["Th"].value],
                    "El": [output.params["El"].value],
                    "Eh": [output.params["Eh"].value],
                    "chisqr": [output.chisqr],
                    "RSS": RSS, 
                    "TSS": TSS, 
                    "Rsq": Rsq,
                    "aic":aic,
                    "bic":bic,
                    "aicc":aicc,
                    "RSS_log": RSS_log,
                    "TSS_log": TSS_log,
                    "Rsq_log": Rsq_log,
                    "aic_log": [output.aic],
                    "bic_log": [output.bic],
                    "aicc_log": aicc_log
                    }
    
        except ValueError:
            pass
        continue

    return(pd.DataFrame(all))

#Schoolfield model simplified, without estimation at low temperature

def Schoolfield_model_nl(id, data, min_round, max_round):
    """ Perform NLLS model fitting on logged trait value with simplified schoolfield model omitting low temperature values """
    #Variabes
    T = np.asarray(data.ConTemp_F[data.id == id]) #Temperature in kelvin
    B = np.asarray(data.OriginalTraitValue[data.id == id]) #Unlogged Trait value
    B_log = np.asarray(data.OriginalTraitValue_log[data.id == id]) #Logged Trait value
    #Starting values
    B0 = data.B0[data.id == id].iloc[0]
    E = abs(data.E[data.id == id].iloc[0])
    Th = data.Th[data.id == id].iloc[0]
    Eh = abs(data.Eh[data.id == id].iloc[0])

    def Schoolfield_residual_nl(p, T, B):
        """ Return residuals for unlogged trait values schoolfield model"""

        B0 = p['B0'].value
        E  = p['E'].value
        Th = p['Th'].value
        Eh = p['Eh'].value

        model = (B0*e**((-E/k)*((1/T)-(1/283.15))))/(
                1+(e**((Eh/k)*((1/Th)-(1/T)))))

        return model - B
    
    def Schoolfield_residual_log_nl(p, T, B_log):
        """ Return residuals for logged trait values and logged schoolfield model """

        B0 = p['B0'].value
        E  = p['E'].value
        Th = p['Th'].value
        Eh = p['Eh'].value

        model = (B0*e**((-E/k)*((1/T)-(1/283.15))))/(
            1+(e**((Eh/k)*((1/Th)-(1/T)))))

        return np.log(model) - B_log

    #Initiate an empty dictionary for results
    all = {"id":[id], 
            "B0":[B0],
            "E":[E],
            "Th":[Th],
            "Eh": [Eh],
            "chisqr":[np.NAN],
            "RSS": [np.NAN],
            "TSS": [np.NAN],
            "Rsq": [np.NAN],
            "aic":[np.NAN],
            "bic":[np.NAN],
            "aicc":[np.NAN],
            "RSS_log": [np.NAN],
            "TSS_log": [np.NAN],
            "Rsq_log": [np.NAN],
            "aic_log": [np.NAN],
            "bic_log": [np.NAN],
            "aicc_log":[np.NAN]
            }
    
    round = 0
    while round < max_round:
    
        round += 1 #Try round +1
        #If TPC converged within mean rounds, or max round is exceeded, break the loop
        if all["aic"] != [np.NaN] and round > min_round: 
            break

        #Starting values defined for rounds
        #For the first round use set starting values
        if round == 1 : 
            #Add starting values
            p = Parameters()
            p.add("B0", value = B0, min = 0)
            p.add("E", value = E, min = 0)
            p.add("Th",Th, min = 250, max = 400)
            p.add("Eh",Eh, min = 0)
        #For other rounds, assign B0 with random number distributed between 0 and B0*2
        else:
            p = Parameters() 
            p.add("B0", value = np.random.uniform(0,B0*2), min = 0)
            p.add("E", value = np.random.uniform(0,E*2), min = 0)
            p.add("Th",Th, min = 250, max = 400)
            p.add("Eh",value = np.random.uniform(0, Eh*2),min = 0)
        #Results for model fitting on logged values
        try:    
            output = minimize(Schoolfield_residual_log_nl, p, args = (T, B_log))
            RSS_log = sum(Schoolfield_residual_log_nl(output.params,T,B_log)**2)
            TSS_log = sum((B_log - np.mean(B_log))**2)
            Rsq_log = 1 - (RSS_log/TSS_log)
            aicc_log = output.aic + 2*4*((4+1)/(len(T) - 4 - 1))
            #Calculate results for unlogged fitting based on parameters estimated from 
            #logged fitting, since they stay the same after log transformation.
            RSS = sum(Schoolfield_residual_nl(output.params,T,B)**2)
            TSS = sum((B - np.mean(B))**2)
            Rsq = 1 - (RSS/TSS)
            aic = len(T)*np.log(RSS/len(T)) + 2*4
            bic = len(T)*np.log(RSS/len(T)) + np.log(len(T))*4
            aicc = aic + 2*4*((4+1)/(len(T) - 4 - 1))

            if aicc < all["aicc"] or all["aicc"] == [np.NaN]:           
                all = {"id":[id], 
                    "B0":[output.params["B0"].value],
                    "E":[output.params["E"].value], 
                    "Th":[output.params["Th"].value],
                    "Eh": [output.params["Eh"].value],
                    "chisqr": [output.chisqr],
                    "RSS": RSS, 
                    "TSS": TSS, 
                    "Rsq": Rsq,
                    "aic":aic,
                    "bic":bic,
                    "aicc":aicc,
                    "RSS_log": RSS_log,
                    "TSS_log": TSS_log,
                    "Rsq_log": Rsq_log,
                    "aic_log": [output.aic],
                    "bic_log": [output.bic],
                    "aicc_log": aicc_log
                    }
        
        except ValueError:
            pass
        continue


    return(pd.DataFrame(all))

#Schoolfield model simplified, without estimation at high temperature

def Schoolfield_model_nh(id, data, min_round, max_round):
    """ Perform NLLS model fitting on unlogged trait value with simplified schoolfield model omitting high temperature values """
    #Variabes
    T = np.asarray(data.ConTemp_F[data.id == id]) #Temperature in kelvin
    B = np.asarray(data.OriginalTraitValue[data.id == id]) #Unlogged Trait value
    B_log = np.asarray(data.OriginalTraitValue_log[data.id == id]) #Logged Trait value
    #Starting values(matters on converge)
    B0 = data.B0[data.id == id].iloc[0]
    E = abs(data.E[data.id == id].iloc[0])
    Tl = data.Tl[data.id == id].iloc[0]
    El = abs(data.El[data.id == id].iloc[0])
    #Add starting values
    p = Parameters()
    p.add("B0", value = B0, min = 0)
    p.add("E", value = E, min = 0)
    p.add("Tl",Tl, min = 250, max = 400)
    p.add("El",El, min = 0)

    def Schoolfield_residual_nh(p, T, B):
        """ Return residuals for unlogged trait values schoolfield model """

        B0 = p['B0'].value
        E  = p['E'].value
        Tl = p['Tl'].value
        El = p['El'].value

        model = (B0*e**((-E/k)*((1/T)-(1/283.15))))/(
                1+(e**((El/k)*((1/Tl)-(1/T)))))

        return model - B
    
    def Schoolfield_residual_log_nh(p, T, B_log):
        """ Return residuals for logged trait values and logged schoolfield model """

        B0 = p['B0'].value
        E  = p['E'].value
        Tl = p['Tl'].value
        El = p['El'].value

        model = (B0*e**((-E/k)*((1/T)-(1/283.15))))/(
            1+(e**((El/k)*((1/Tl)-(1/T)))))

        return np.log(model) - B_log

    #Initiate an empty dictionary for results
    all = {"id":[id], 
            "B0":[B0],
            "E":[E],
            "Tl":[Tl],
            "El": [El],
            "chisqr":[np.NAN],
            "RSS": [np.NAN],
            "TSS": [np.NAN],
            "Rsq": [np.NAN],
            "aic":[np.NAN],
            "bic":[np.NAN],
            "aicc":[np.NAN],
            "RSS_log": [np.NAN],
            "TSS_log": [np.NAN],
            "Rsq_log": [np.NAN],
            "aic_log": [np.NAN],
            "bic_log": [np.NAN],
            "aicc_log": [np.NAN]
            }
    
    round = 0
    while round < max_round:
    
        round += 1 #Try round +1
        #If TPC converged within mean rounds, or max round is exceeded, break the loop
        if all["aic"] != [np.NaN] and round > min_round:  
            break

        #Starting values defined for rounds
        #For the first round use set starting values
        if round == 1 : 
            #Add starting values
            p = Parameters()
            p.add("B0", value = B0, min = 0)
            p.add("E", value = E, min = 0)
            p.add("Tl",Tl, min = 250, max = 400)
            p.add("El",El, min = 0)
        #For other rounds, assign B0 with random number distributed between 0 and B0*2
        else:
            p = Parameters() 
            p.add("B0", value = np.random.uniform(0,B0*2), min = 0)
            p.add("E", value = np.random.uniform(0,E*2), min = 0)
            p.add("Tl",Tl, min = 250, max = 400)
            p.add("El", value = np.random.uniform(0, El*2),min = 0)

        #Results for model fitting on logged values
        try:    
            output = minimize(Schoolfield_residual_log_nh, p, args = (T, B_log))
            RSS_log = sum(Schoolfield_residual_log_nh(output.params,T,B_log)**2)
            TSS_log = sum((B_log - np.mean(B_log))**2)
            Rsq_log = 1 - (RSS_log/TSS_log)
            aicc_log = output.aic + 2*4*((4+1)/(len(T) - 4 - 1))
            #Calculate results for unlogged fitting based on parameters estimated from 
            #logged fitting, since they stay the same after log transformation.
            RSS = sum(Schoolfield_residual_nh(output.params,T,B)**2)
            TSS = sum((B - np.mean(B))**2)
            Rsq = 1 - (RSS/TSS)
            aic = len(T)*np.log(RSS/len(T)) + 2*4
            bic = len(T)*np.log(RSS/len(T)) + np.log(len(T))*4
            aicc = aic + 2*4*((4+1)/(len(T) - 4 - 1))

            if aicc < all["aicc"] or all["aicc"] == [np.NaN]:           
                all = {"id":[id], 
                    "B0":[output.params["B0"].value],
                    "E":[output.params["E"].value], 
                    "Tl":[output.params["Tl"].value],
                    "El": [output.params["El"].value],
                    "chisqr": [output.chisqr],
                    "RSS": RSS, 
                    "TSS": TSS, 
                    "Rsq": Rsq,
                    "aic":aic,
                    "bic":bic,
                    "aicc":aicc,
                    "RSS_log": RSS_log,
                    "TSS_log": TSS_log,
                    "Rsq_log": Rsq_log,
                    "aic_log": [output.aic],
                    "bic_log": [output.bic],
                    "aicc_log": aicc_log
                    }
        
        except ValueError:
            pass
        continue


    return(pd.DataFrame(all))


#Initialise empty dataframs for fitting results

Cubic = pd.DataFrame(data = None)
Briere = pd.DataFrame(data = None)
Schoolfield = pd.DataFrame(data = None)
Schoolfield_nl = pd.DataFrame(data = None)
Schoolfield_nh = pd.DataFrame(data = None)

###NLLS model fitting
print("Start model fitting with NLLS")
t0 = datetime.now()


#Cubic model
print("Cubic model fitting")
i = 0
t = datetime.now()
for id in data["id"].unique():
    Cubic = Cubic.append(Cubic_model(id, data))
    #Print proceeding percentage to screen
    i += 1
    percent = i / len(data.id.unique())
    sys.stdout.write("\rCompleted: %.2f%%" % (percent*100))
#Calculate and print the converge ratio of TPCs
fail = Cubic.aic.isnull().sum()
converge = len(Cubic.id) -  fail
print("\nDone! %i out of %i TPCs are converged with Cubic model"%(converge, len(Cubic.id)))
print("Time taken:", datetime.now() - t)

#Briere model
i = 0
t = datetime.now()
print("Briere model fitting")
for id in data["id"].unique():
    Briere = Briere.append(Briere_model(id, data,3, 25))
    #Print proceeding percentage to screen
    i += 1
    percent = i / len(data.id.unique())
    sys.stdout.write("\rCompleted: %.2f%%" % (percent*100))
#Calculate and print the converge ratio of TPCs
fail = Briere.aic.isnull().sum()
converge = len(Briere.id) - fail
print("\nDone! %i out of %i TPCs are converged with Briere model"%(converge, len(Briere.id)))
print("Time taken:", datetime.now() - t)

#Schoolfield model
i = 0
t = datetime.now()
print("Schoolfield model fitting")
for id in data["id"].unique():
    Schoolfield = Schoolfield.append(Schoolfield_model(id, data,3, 25))
    #Print proceeding percentage to screen
    i += 1
    percent = i / len(data.id.unique())
    sys.stdout.write("\rCompleted: %.2f%%" % (percent*100))
#Calculate and print the converge ratio of TPCs
fail = Schoolfield.aic.isnull().sum()
converge = len(Schoolfield.id) - fail
print("\nDone! %i out of %i TPCs are converged with Schoolfield model"%(converge, len(Schoolfield.id)))
print("Time taken:", datetime.now() - t)

#Schoolfield model without low temperature parameters
i=0
t = datetime.now()
print("Schoolfield model without low temperature parameters fitting")
for id in data["id"].unique():
    Schoolfield_nl = Schoolfield_nl.append(Schoolfield_model_nl(id, data,3, 25))
    #Print proceeding percentage to screen
    i += 1
    percent = i / len(data.id.unique())
    sys.stdout.write("\rCompleted: %.2f%%" % (percent*100))
#Calculate and print the converge ratio of TPCs
fail = Schoolfield_nl.aic.isnull().sum()
converge = len(Schoolfield_nl.id) - fail
print("\nDone! %i out of %i TPCs are converged with Schoolfield_nl model"%(converge, len(Schoolfield_nl.id)))
print("Time taken:", datetime.now() - t)

#Schoolfield model without high temperature parameters
i = 0
t = datetime.now()
print("Schoolfield model without high temperature parameters fitting")
for id in data["id"].unique():
    Schoolfield_nh = Schoolfield_nh.append(Schoolfield_model_nh(id, data,3, 25))
    #Print proceeding percentage to screen
    i += 1
    percent = i / len(data.id.unique())
    sys.stdout.write("\rCompleted: %.2f%%" % (percent*100))
#Calculate and print the converge ratio of TPCs
fail = Schoolfield_nh.aic.isnull().sum()
converge = len(Schoolfield_nh.id) - fail
print("\nDone! %i out of %i TPCs are converged with Schoolfield_nh model"%(converge, len(Schoolfield_nh.id)))
print("Time taken:", datetime.now() - t)

print("\nTotal time spent:", datetime.now() - t0)

#Save results
print("Saving results")
Cubic.to_csv("../results/Cubic_model.csv")
Briere.to_csv("../results/Briere_model.csv")
Schoolfield.to_csv("../results/Schoolfield.csv")
Schoolfield_nl.to_csv("../results/Schoolfield_nl.csv")
Schoolfield_nh.to_csv("../results/Schoolfield_nh.csv")
print("Done!")

