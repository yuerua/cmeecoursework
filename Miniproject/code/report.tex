\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[margin=2cm]{geometry}
\usepackage[onehalfspacing]{setspace}
\usepackage{lineno}
\usepackage{graphicx}
\usepackage{helvet}
\renewcommand{\familydefault}{\sfdefault}
\usepackage{setspace}
\usepackage{indentfirst}
\usepackage{siunitx}
\usepackage{amssymb}
\usepackage{parskip}
\usepackage{lineno}
\usepackage[labelfont=bf]{caption}
\usepackage{float}
\usepackage{csvsimple}
\usepackage[comma]{natbib}

\begin{document}

	\begin{titlepage}
		
		\begin{center}
			
		\vspace*{1cm}
		\LARGE
		Imperial College London\\
		\vspace*{1cm}
		\LARGE
		CMEE MiniProject\\
		
		\vspace*{3cm}
		\Huge
		\textbf{Evaluating the Performance of Mechanistic and Phenomenological Models on the Thermal Response of Different Trait Types}\\
		
		\vspace{5cm}
		\Large
		
		\textbf{Student:} Hanyun Zhang \textit{(hanyun.zhang18@imperial.ac.uk)}\\
		\vspace*{1cm}
		\textbf{CID:} 01557378\\
		\vspace*{1cm}
		\textbf{Word Count:} 3485
		
		\vspace{2cm}
		
		\end{center}

	\end{titlepage}
	
	\clearpage
	
	\linenumbers
	\doublespacing
    \section*{Abstract}
Modeling thermal response of biotic traits promotes the understanding of the mechanism behind thermal ecology and metabolic adaptation. Over the years many models were proposed attempting to describe the temperature dependence of biological processes, but their performance may be diverse across studies. To investigate the potential factors affecting the applicability of models, two phenomenological models including the Cubic polynomial model and the Briere model, and three mechanistic models including the general Schoolfield model and two derivatives without low or high temperature estimation were fitted on a database of thermal response varied in species, habitats and traits. Model selection was performed with three information criteria considering both fitting quality and parsimony. As judged by the small sample unbiased AIC (AICc), the phenomenological models performed better for more thermal response curves than the mechanistic models, and there was no significant effect of trait types on model selection.

\section{Introduction}
Temperature is among the most important factors driving the dynamic change of biotic traits. As proposed by the metabolic theory, the temperature dependence of metabolic rates serves as a foundation for the thermal response of higher level biological processes \citep{brown2004toward}. In order to describe the relationship between metabolic rates and temperature, multiple models have been designed attempting to fit a wider range of the thermal performance curve(TPC) \citep{delong2017combined}. For example, the Boltzmann-Arrhenius equation (eq. \ref{eq1}) models the response of metabolic rate $B$ to temperature $T$ by multiplying the metabolic rate at a reference temperature $B_0$ by the Arrhenius factor, $e^{-\frac{E}{kT}}$, where $T$ is the absolute temperature, $k$ is the Boltzmann’s constant and $E$ is the activation energy of an enzymatic reaction \citep{gillooly2001effects}. 
		\begin{equation}
		\label{eq1}
		B = B_0e^{-\frac{E}{kT}}
        \end{equation} 
Since the exponent is a monotonically increasing function of temperature, the Boltzmann-Arrhenius equation fails to describe the peak and decline of metabolic rate when the temperature continues to rise above the optimal temperature, which restrains its performance in more variable climates \citep{schulte2015effects}. To better model the unimodal shape of the TPCs, the classic Boltzmann-Arrhenius model is refined based on the assumption that enzymes inactivate at low temperature as well as at the temperature higher than the optimal temperature \citep{schoolfield1981non}. Apart from mechanistic models, there are also phenomenological models aims at describing the shape of TPCs without considering the biological meaning of parameters.\\
To evaluate the model performance and identify the best model to depict the thermal response under different circumstance, several model selection criterion are employed, including the Akaike information criterion (AIC), the small sample unbiased AIC (AICc) and the Bayesian information criterion (BIC) \citep{johnson2004model}. All of the three approaches comprise a goodness-of-fit term and a penalty term related to model complexity. As such a smaller value of the criteria indicates a better model \citep{johnson2004model}. AICc is a derivative of AIC that includes a bias correction term for small sample size. Both AIC and AICc estimate the Kullback-Leibler information lost and select the model best approximates the true model \citep{burnham2004multimodel}. BIC is structurally similar to the other two methods but is based upon the assumption that a true model exists in the given model set \citep{burnham2004multimodel}. Since AICc is recommended for sample size smaller than 40 times of free parameters, which fits the situation for most curves in the available dataset, it is given the first priority in model selection for this study \citep{burnham2004multimodel}. The result of AICc was compared with that of the AIC and BIC in order to investigate how the judgment of the best model is affected by the model selection criteria.

\section{Methods}
    \subsection{Models}
Five models were fitted on the dataset:\\
The general cubic polynomial model (eq. \ref{eq2}) is a phenomenological model capturing the shape of TPCs without any biological interpretation in its parameters. The equation gives the value of trait $B$ at the given temperature $T$ measured in Celsius.

		\begin{equation}
		\label{eq2}
		B = B_0 + B_1T + B_2T^2 + B_3T^3
        \end{equation} 

The Briere model (eq. \ref{eq3}) is also phenomenological, with $B_0$ being an empirical constant, and $T_0$, $T_m$ being the minimum and maximum temperature thresholds for positive trait values respectively \citep{briere1999novel}. The equation gives the relationship between trait $B$ and temperature $T$ measured in Celsius. 
		
		\begin{equation}
		\label{eq3}
		B = B_0 T (T-T_0) \sqrt{T_m-T}
        \end{equation} 

The Schoolfield model (eq. \ref{eq4})  is a mechanistic model based on the assumption that the development rate of an organism is dominated by a reversible deactivation of a single enzyme at low and high temperatures \citep{schoolfield1981non}. The equation gives the value of trait $B$ at a given absolute temperature $T$ measured in Kelvin. $B_0$ represents the trait value at the reference temperature, which is defined as 283.15K in this study. $E$ is the activation energy of the enzymatic reaction. $E_l$ and $E_h$ are the deactivation energy for the enzyme at low and high temperatures. $T_l$ and $T_h$ are the temperatures at which the enzyme activity is 50\% deducted due to low and high temperatures respectively. $k$ is the Boltzmann constant, which equals to $8.617 \times 10^{-5}$eV$\cdot$K$^{-1}$. All the temperatures are measured in Kelvin in the equation. 
		\begin{equation}
		\label{eq4}
        B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}{ 1 + e^{\frac{E_l}{k} (\frac{1}{T_l}- \frac{1}{T})} + e^{\frac{E_h}{k} (\frac{1}{T_h} - \frac{1}{T})}}
        \end{equation} 
In cases where the inactivation at low or high temperatures are weak, or there were no adequate measurements at low or high temperatures, two derivatives of the Schoolfield model were used, with each of them excluding the parameters for low or high temperatures (No Low Schoolfield and No High Schoolfield model).  
		\begin{equation}
		\label{eq5}
		B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}
        { 1 +  e^{\frac{E_h}{k} (\frac{1}{T_h} - \frac{1}{T})}}
        \end{equation}
        \begin{equation}
        \label{eq6}
        B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}
        { 1 +  e^{\frac{E_l}{k} (\frac{1}{T_l} - \frac{1}{T})}}    
        \end{equation}
\subsection{Data sorting}
The data was taken from the Biotraits database \citep{della2013thermal} which contained thermal responses from 2165 studies. The measurements covered various metabolic traits such as growth rate, photosynthesis rate, and respiration rate, for species ranging from microbes, plants to vertebrates. To allow TPCs to be better fitted with log-linear models, only the positive trait values were maintained. Meanwhile, groups with sample size less than 8 were excluded, because the maximum parameter number among the five models was 6, and therefore at least 8 data points were required for Non-linear least squares (NLLS) fitting and downstream assessments \citep{newville2016lmfit}\citep{johnson2004model}. 

\subsection{Starting values}
Starting values of parameters were calculated before the model fitting in order to reduce iterations required for convergence. For the Cubic model, starting values of $B_0$, $B_1$, $B_2$, and $B_3$ were all set to zero without additional estimation because they lacked biological meaning. Similarly, $B_0$ of the Briere model was set to 0.01 at the beginning, while $T_0$ and $T_m$ were assigned with the minimum and maximum temperature measured in the experiment respectively. For the Schoolfield model, $B_0$ was estimated as the trait value at temperature closest to 10 $^{\circ}$ C. $T_l$ was estimated as the average of the minimum temperature and the optimal temperature at which the trait value peaks, while $T_h$ was calculated as the average of the maximum and the optimal temperature. To obtain the estimation of $E$, $E_l$, and $E_h$, the equation was converted to a linear relationship between log-transformed trait value and $\frac{1}{kT}$ \citep{schoolfield1981non}. Then $E$ was taken as the slope of the linear regression of points after the peak and $E_h$ the slope before the peak. $E_l$ was assigned with 0.5$\times E$. In cases where there were too few points before or after the peak to perform the linear regression, the starting value of $E$, $E_h$ was set as default to 0.6, since the activation energies for a large proportion of biological processes were estimated to 0.6-0.7 eV \citep{gillooly2001effects}. 
\subsection{Model fitting}
Each of the five models was fitted on 992 TPCs with the NLLS method using the LMFIT package in python \citep{newville2016lmfit}. The method estimates the parameter values in order to minimize the residual between actual data and the given model with the Levenberg-Marquardt algorithm. Both of the Cubic model and the Briere model were fitted on the original data, whereas the three versions of the Schoolfield model were fitted on the log-transformed values. Except for the Cubic model, each model was fitted at least three times with different starting values. For the first trial, the starting values were set as the ones calculated from the data. Then two more attempts were performed with starting values randomized between zero and two times of the calculated value (between 0 and 1 for the Briere model). The trial with the minimum AICc was taken as the best fit. If the model didn’t converge within the three attempts, a maximum of 22 more trials were performed.

\subsection{Model selection}
Multiple model selection criteria were calculated for comparison, including AIC, BIC, AICc and R squared. For the Cubic and the Briere model, the AIC and BIC values were calculated by the "minimize" function with the residuals on the un-log scale. Whereas residuals of the three versions of the Schoolfield model were first converted to the un-log scale by applying the parameters estimated from log-transformed data to the un-log model. Then the AIC, BIC, and AICc were calculated using the equations below, where $RSS$ is the sum of the squares of residuals, $p$ is the number of free parameters of a model, and $n$ is the sample size \citep{johnson2004model}\citep{newville2016lmfit}.
		\begin{equation}
		AIC = n\ln{\frac{RSS}{n}} + 2p
        \end{equation}
        
        \begin{equation}
		BIC = n\ln{\frac{RSS}{n}} + p\ln(n)
        \end{equation}
        
        \begin{equation}
		AICc = AIC + {\frac{2p(p+1)}{n-p-1}} 
        \end{equation}
For model comparison, the $\Delta$ value and the $W_i$ of AIC, BIC, and AICc were calculated for each model separately. The $\Delta$ value was computed as the difference between the AIC/BIC/AICc score of each model and the lowest score among all models. Models with $\Delta$ value equal or less than 2 were selected as comparable best models by certain criterion. To assess the probability of a model to be the best model, $\Delta$ values were converted into the relative $W_i$ using the equation below. 
		\begin{equation}
		W_i = \frac{\exp\{-\frac{1}{2}\Delta_i\}}{\sum_{k=1}^{K} \exp\{-\frac{1}{2}\Delta_k\}}
        \end{equation}
$W_i$ refers to relative weights of evidence that support the model to be the best model assessed by AIC/BIC/AICc. It is computed as the normalized model likelihood of a single model among a given model set, such that the $W_i$ of all models sum to 1\citep{burnham2004multimodel}. Models with the highest $W_i$ were selected as the best model, and a $W_i$ over 0.9 suggested an overwhelming support by the data \citep{johnson2004model}\citep{burnham2004multimodel}.\\
In order to interpret the biological effects on model performance, the data were categorized into three subsets based on trait types: the respiration rate, photosynthesis rate, and growth rate. Model comparison was performed with $W_i$ for each subset respectively. The variation of $W_i$ was first assessed by the ANOVA test and then the Tukey HSD test was employed for pair-wise comparisons. 

\subsection{Computing languages}
\begin{itemize}
\item R 3.5.1 was used for data sorting and starting value estimation before the fitting of models, as data wrangling was convenient with multiple inbuilt functions and libraries in R. The analysis and visualization of model selection results were also completed in R with ggplot2 and gridExtra. 
\item Python 3.6.3 was used for model fitting with the LMFIT package, as iteration in python ran fast in dealing with a large amount of data.
\item Bash 4.3.48 was used for compiling the project into a workflow and to produce the PDF report with LaTeX.
\end{itemize}

\section{Results}
Sorting of the original database resulted in 992 groups (about 46\% of the original dataset) with a mean sample size of about 18. Distribution of the sample size of three trait type groups was shown in fig.\ref{fig2}. During the model fitting process, all groups converged on the Cubic and the No Low Schoolfield model. Whereas for the Briere model, the full Schoolfield model, and the No High Schoolfield model there were 14, 2, and 1 curves failed to converge, respectively. The mean of R squared, AIC, BIC and AICc values for each converged model were shown in table.\ref{tb1}. The Cubic model scored highest for averaged R squared, and had the lowest mean AIC, BIC and AICc, indicating that it tended to provide the best fit.

\begin{table}[H] 
        \centering 
        \caption{Converged TPCs and mean AIC, BIC, AICc and R squared for each fitted model. The Cubic model scored lowest for AIC, BIC and AICc, and had the highest mean R squared.}
		    \label{tb1}
        \csvautotabular{../results/tb1.csv}
		
\end{table}

Fig.\ref{fig1} shows five example TPCs with one of the five models selected as the best model based on the lowest AICc value. The score of AIC, BIC, AICc, and R squared of the first curve were shown in table.\ref{tb2}. It is clear that the three criteria were not always consistent in selecting the best model.

\begin{figure}[H]\centering
		\includegraphics[width = \textwidth]{../results/figure1.pdf}
		\caption{The five TPCs with fitting of models. Plots from A to E showed examples for each of the five models (Cubic, Briere, full Schoolfield, No Low Schoolfield, and No High Schoolfield) selected as the best model by AICc. Data for the plots were taken from five individual studies \citep{ouedraogo1997effect}\citep{vidal1997intraspecific}\citep{badger1982analysis}\citep{thebud1982effects}\citep{davey1994modelling}.}
		\label{fig1}
\end{figure}

\begin{table}[H] \centering 
		\caption{AIC, BIC, AICc and R squared for each model fitted on the TPC shown in figure \ref{fig1}(A). The Schoolfield model was selected as the comparable best model by AIC and BIC, but not AICc.}
		\label{tb2}
    \csvautotabular{../results/tb2.csv}
\end{table}

\begin{figure}[H]\centering
		\includegraphics[width = \textwidth]{../results/figure2.pdf}
		\caption{The density plot of sample size. The majority of groups had a sample size smaller than 120, as shown by the part before the vertical line.}
		\label{fig2}
\end{figure}

The AICc, BIC, and AIC estimate the model parsimony by considering both fitting quality and model complexity, while R squared measures only the fit and tends to favor fuller models. Since the majority of data had a small sample size (smaller than 40 times of the minimum parameter number) (fig.\ref{fig2}), the AICc was chosen as the primary method for model comparison. A $\Delta$AICc equal to or smaller than 2 indicates strong support for the model \citep{burnham2004multimodel}. The percentage of $\Delta$AICc $<=$2 for each model was shown in fig.\ref{fig3}, with the comparison across three trait types. It was clear that the two phenomenological models, Cubic and Briere, were selected as the best model or the comparable best model for over 50\% of the curves in all subsets. The two simplified versions of the Schoolfield model ranked similarly in all subsets, while the full Schoolfield model was rarely selected as the best model, especially in the respiration group. 

\begin{figure}[H]\centering
		\includegraphics[width = \textwidth]{../results/figure3.pdf}
		\caption{Stacked bar graph showing the percentage of TPCs with $\Delta$AICc$<=$2 for each model within respective trait type groups. The two phenomenological models performed better in more curves than the three mechanistic models in all trait type groups}
		\label{fig3}
\end{figure}

The comparison of $W_i$(AICc) showed a similar result (fig.\ref{fig4}), where the Cubic model and the Briere model had higher $W_i$(AICc) in more curves and therefore were selected as the best model more often. The Cubic model showed a comparable distribution with the Briere model in the growth subset (p$>$0.001) and ranked higher than the Briere model in the other trait type groups. The full Schoolfield model had the lowest mean $W_i$(AICc) in the photosynthesis and respiration groups, while for the growth group there was no significant difference between the $W_i$(AICc) of three versions of the Schoolfield model (p$>$0.001).

\begin{figure}[H]\centering
		\includegraphics[width = \textwidth]{../results/figure4.pdf}
		\caption{The distribution of $W_i$(AICc) for each model within respective trait type groups. The two phenomenological models had higher $W_i$(AICc) in more curves than the three mechanistic models in all trait type groups}
		\label{fig4}
\end{figure}

However, the ranking of phenomenological and mechanistic models were differed by trait types when $W_i$(AIC) and $W_i$(BIC) were employed (fig.\ref{fig5}). For both criteria, the full Schoolfield Model had a higher $W_i$ in more curves than the Briere model and the two simplified Schoolfield Model in the growth and photosynthesis subset (p$<$0.001) and showed a comparable $W_i$ distribution with the Cubic model in the photosynthesis subset (p$>$0.001). While in the respiration subset the full Schoolfield Model was the least frequently selected best model among the model set (p$<$0.001).

\begin{figure}[H]\centering
		\includegraphics[width = \textwidth]{../results/figure5.pdf}
		\caption{The distribution of $W_i$(AICc), $W_i$(AIC), and $W_i$(BIC) for each model within respective trait type groups. The full Schoolfield model was targetted more often by AIC and BIC in growth and photosynthesis groups.}
		\label{fig5}
\end{figure}

\section{Discussion}
The Cubic model was targeted most or comparable most frequently as the best model by AICc for all trait type groups including the growth rate, the photosynthesis rate, and the respiration rate. The less performance of the Briere model was likely caused by the lack of fit, because the parameter number of Briere model was 1 less than the Cubic model, and therefore had a smaller penalty term of AICc. The relative poor fitting quality of Briere model was also revealed by the lowest averaged R squared (table.\ref{tb1}).It was not surprising that the Cubic model fitted better on more curves, because the polynomial model was designed to capture the successive bends of the curve, and was likely to perform better for irregular TPC than the models with parameter values limited to a defined range. Among the three mechanistic models, AICc tended to favor the two simplified version of Schoolfield model over the general model. This was partly attributed to the strong penalty term provided by AICc when the sample size was small. Alternative explanation relied on the properties of TPCs in the available dataset. It was likely that a large proportion of data groups lacked measurements below or above the peak of TPCs. In either case, the simplified Schoolfield models were adequate to fit, and the full Schoolfield model was penalized for overfitting \citep{johnson2004model}.\\
As judged by the $W_i$(AICc), the ranking of models was consistent across trait type groups, indicating that the difference in trait types didn’t have a significant effect on the chosen of a better-supported model. From a biological point of view, photosynthesis and respiration provide energy and material for the growth of organisms, and thereby contribute to the population density and even the structure of ecological systems \citep{brown2004toward}. The intrinsic link between metabolic rate and higher order biological processes may explain the similarity in model selection for different trait types. When an organism was exposed to a certain temperature range, different biological rates might show a similar relationship with temperature depending on the thermal response of fundamental biochemical reaction rates \citep{brown2004toward}. Therefore, the difference in trait types didn’t cause significant bias in model selection. Further exploration into the characteristics of TPCs favored by a particular model may reveal the appropriate model and theory for the temperature response under different conditions.\\
Additionally, the mechanistic models had relative lower $W_i$(AICc) in a vast majority of curves, even if they had the same parameter numbers as the Cubic model, which suggested a greater loss of information during the fitting of mechanistic models \citep{johnson2004model}. The Schoolfield model was based on the assumption that the thermal response of traits was governed by a single dominant enzymatic reaction \citep{schoolfield1981non}. However, biological processes in reality comprised multiple metabolic reactions and could be impacted by various factors. As a result, the fitting performance of the Schoolfield model might be limited by the complexity of thermal response in reality, especially under less controlled conditions. An investigation into important factors influencing the performance of mechanistic models could promote the mechanistic understanding of the temperature dependence of biotic traits.\\
In contrast to the results of $W_i$(AICc), the rankings of $W_i$(AIC) and $W_i$(BIC) varied between trait types. Most strikingly, AIC and BIC tended to target the full Schoolfield model more frequently in growth and photosynthesis group than AICc did, and to select the Briere model less frequently in photosynthesis and respiration group. The variation was led by the difference in penalty levels of the three criteria. With small sample size, AICc was least tolerant of free parameters and tended to select a simpler model. For models with 6 parameters, the penalty term of AICc was larger than that of BIC and AIC when sample size was smaller than 21 (fig.\ref{fig6}). As the sample size increase, AICc gradually reduced to AIC while BIC would continually increase. As a vast majority of curves had a sample size smaller than 21 (Fig.\ref{fig2}), the $\Delta$AICc of Schoolfield model was larger than $\Delta$AIC and $\Delta$BIC for most curves, which explained the extremely low $W_i$(AICc) scored by the full Schoolfield model. 

\begin{figure}[H]\centering
		\includegraphics[width = \textwidth]{../results/figure6.pdf}
		\caption{The correlation between sample size and the penalty term of three model selection criteria with a given parameter number of 6. AICc had the largest penalty term when sample size was smaller than 21.}
		\label{fig6}
\end{figure}

On the other hand, though the rankings of $W_i$(AIC) and $W_i$(BIC) were different to that of $W_i$(AICc), they were similar to each other in all trait type subsets (fig.\ref{fig5}). This suggested that AIC and BIC tended to select the same model for a majority of TPCs. Since the general AIC lacked a bias correction for small sample size, and BIC might target underfit model when sample size was small, model selection performed by AICc was considered to be more reliable in this study \citep{burnham2004multimodel}. Besides, the consistent ranking of $W_i$(AICc) in different trait type groups was in accordance with the metabolic theory that the thermal response pattern was not significantly distinguished by trait types \citep{brown2004toward}. Given that AIC and BIC were based upon different theorems, the choice of appropriate approach should not only consider the sample size but also other properties of data sets and the objective of model comparison. For instance, AIC and AICc were more accurate than BIC in evaluating the predictive performance of a model when the datasets were less heterogenetic \citep{brewer2016relative}, and BIC was more consistent in finding the most parsimonious model in regardless of sample size \citep{burnham2004multimodel}. With the variant qualities of the available data and the limited information given by small sample sizes, AICc was not always reliable in selecting the best model. As such, whether there was a preference for model depending on trait types should be carefully verified. Diagnosis of outliers and leverage points, filtering datasets with larger sample size, and combining AICc and BIC results could better reveal the performance of models for different trait types.\\
This study fitted five models on various TPCs and compared their performance for different trait types. As judged by $W_i$(AICc), the phenomenological models were more frequently selected as the best model. By contrast, AIC and BIC favored the full Schoolfield Model more often than the others for TPCs of photosynthesis rate and growth rate. Given the different behaviors of three model selection criteria, further study is required to resolve the effect of trait types on the choice of better model.  

\bibliographystyle{agsm}
\setcitestyle{authoryear,open={(},close={)}}
\bibliography{References}
\end{document}
