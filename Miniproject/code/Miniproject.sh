#!/bin/bash
# Author: Hanyun Zhang (hanyun.zhang18@imperial.ic.uk)
# Desc: Runs the workflow for CMEE miniproject, compile into a pdf report

echo -e "\nClear results\n"
rm -rf ../results/*i

echo -e "\nSorting data with sortdata.R\n"
Rscript sortdata.R
echo -e "\n##############################################"

echo -e "\nPerforming model fitting with NLLS.py\n"
python3 NLLS.py
echo -e "\n##############################################"

echo -e "\nGenerating the results with plot_type.R\n"
Rscript plot_type.R
echo -e "\n##############################################"

echo -e "\nCompiling report\n"
pdflatex report.tex 
bibtex report    
pdflatex report.tex 
pdflatex report.tex

mv report.pdf ../results/

## Cleanup
rm *.aux
rm *.log
rm *.bbl
rm *.blg
rm Rplots.pdf
echo -e "\n##############################################"

echo -e "\nDone! \n"

