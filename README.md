# == CMEE Coursework Repository 2018-2019 ==
![Alt text](http://news.mit.edu/sites/mit.edu.newsoffice/files/images/2015/MIT-Data-Sci-Machine_0.jpg)
# Overview
  Include scripts and data for [CMEE coursework](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/Index.ipynb), sorting by weeks.  
  Author: Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)  
  Date: 2018-2019  
  Input files are in sandbox or data directory.Output files are saved to results.  
  Please find detailed description in weekly README.txt.

# Contents
## [Week1](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week1/)
####[Introduction to Unix](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/01-Unix.ipynb)
####[Shell scripting](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/02-ShellScripting.ipynb)
  * Command arguments
  * Redirection and pipes
  * Wildcards
  * Using grep
  * Finding files
####[Version Control with Git](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/03-Git.ipynb)
####[Scientific documents with LATEX](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/04-LaTeX.ipynb)

## [Week2](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week2/)
####[Biological Computing in Python-I](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/05-Python_I.ipynb)
  * Python basics
  * Python Input/Output
  * Python programs
  * Control statements
  * Debugging

## [Week3](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week3/)
####[Biological Computing in R](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/07-R.ipynb)
  * R basics
  * Vectorization
  * Debugging
  * Sweave and knitr
  * Data wrangling
  * Data visualization
  * qplot and ggplot
 
## [Week7](https://bitbucket.org/yuerua/cmeecoursework/src/master/Week7/)
####[Biological Computing in Python-II](http://nbviewer.jupyter.org/github/mhasoba/TheMulQuaBio/blob/master/notebooks/06-Python_II.ipynb)
  * Manipulation of scipy arrays
  * Plotting with matplotlib
  * Profiling with run -p and timeit
  * Networks
  * Regular expressions
  * Build workflows

# Dependencies
 
### Ubuntu 16.04
 
### Python 3.6.3
####Modules
  * pickle
  * numpy
  * scipy
  * pandas
  * networkx
  * matplotlib
 
### R 3.5.1
####Packages
  * lattice
  * reshape2
  * dplyr
  * tidyr
  * ggplot2
  * maps
  * igraph
  * knitr

### LaTeX



     


