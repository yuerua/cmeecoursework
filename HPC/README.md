#=== MY CMEE Coursework Directory - HPC Practical ===  
  
Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk  
Date: Jan 2019  
  
##== Description ==  

Include the script and result for the HPC practicals.  

##Files location tips
###[PDF document --- HPC/LaTeX/hz5418.pdf](https://bitbucket.org/yuerua/cmeecoursework/src/2033e64f127ca471489e5d4663ef2a28ab58f9b6/HPC/LaTex/hz5418.pdf?at=master&fileviewer=file-view-default)  
###[R file --- HPC/code/hz5418.R](https://bitbucket.org/yuerua/cmeecoursework/src/2033e64f127ca471489e5d4663ef2a28ab58f9b6/HPC/code/hz5418.R?at=master&fileviewer=file-view-default)  
###[zip file --- HPC/results/hz5418_Cluster.zip](https://bitbucket.org/yuerua/cmeecoursework/src/2033e64f127ca471489e5d4663ef2a28ab58f9b6/HPC/results/hz5418_Cluster.zip?at=master&fileviewer=file-view-default)  

Input files are in /results/Cluster    
Output files are saved to /results      
  
== List of files ==               == Description == 

├── code
│   ├── Cluster.R                 # R script for running the neutral theory model on HPC 
│   ├── hz5418.R                  # R code for HPC exercises worksheet
│   └── run_cluster.sh            # Shell script to run Cluster.R
├── data
├── LaTex
│   ├── figure
│   ├── hz5418.pdf                # Containing figures and answers to star questions
│   └── hz5418.Rnw
├── results
│   ├── Cluster
│   └── hz5418_Cluster.zip        # Containing results from cluster and codes to produce them
└── sandbox
