
#SCRIPT 2: Trimming and filtering all fastq files, with trimLeft=30 for both F and R reads

##Important! To save time, always restart the R session before continue 

#Just using trimL=30 to trim primers from start of each sequence, as seemed to work ok on run2b trial
#Note - worked in two batches according to fastq file naming convention as easiest that way: 
#Batch1 (LIMS 6386) _R1_001.fastq 
#Batches 2, 2b and 3 R1.fastq

rm(list=ls())

library(dada2)
packageVersion("dada2")

setwd("/home/hanyun/CMEECourseWork/Mainproject/code/")
path <- "/home/hanyun/CMEECourseWork/Mainproject/data/Vargas/" # directory containing the fastq files after unzipping
#path <- "/home/hanyun/CMEECourseWork/Mainproject/data/Vargas/weak_failed" 
list.files(path)
#BATCH 1 LIMS 6386 only ====
# Sort ensures forward/reverse reads are in same order
fnFs <- sort(list.files(path, pattern="_R1_"))
fnRs <- sort(list.files(path, pattern="_R2_"))
length(fnFs)
length(fnRs) #both 269 as expected

# Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq
sample.names <- sapply(strsplit(fnFs, "_"), `[`, 1)
# Specify the full path to the fnFs and fnRs
fnFs <- file.path(path, fnFs)
fnRs <- file.path(path, fnRs)

filt_path <- file.path(path, "filtered") # Place filtered files in filtered/ subdirectory
filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt_trimL.fastq.gz"))
filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt_trimL.fastq.gz"))
#If error occurs, restart Rstudio, probably due to multithread
#Time consuming about (10''ish)
out1 <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, truncLen=c(250,200),
                      maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE, trimLeft = 30,
                      compress=TRUE, multithread=TRUE)
head(out1)

write.csv(out1, "/home/hanyun/CMEECourseWork/Mainproject/results/trimfilter_out.csv")
saveRDS(out1, "/home/hanyun/CMEECourseWork/Mainproject/results/trimfilter_out.rds")

out2 <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, truncLen=c(250,200),
                      maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE, trimLeft = 30,
                      compress=TRUE, multithread=TRUE)
write.csv(out2, "/home/hanyun/CMEECourseWork/Mainproject/results/Main/trimfilter_out_weak_failed.csv")
saveRDS(out2, "/home/hanyun/CMEECourseWork/Mainproject/results/Main/trimfilter_out_weak_failed.rds")
#out2 <- filterAndTrim(fnFs[135:length(fnFs)], filtFs[135:length(fnFs)], fnRs[135:length(fnFs)], filtRs[135:length(fnFs)], truncLen=c(250,200),
#                      maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE, trimLeft = 30,
#                      compress=TRUE, multithread=TRUE)
#head(out2)
#write.csv(out1, "/home/hanyun/CMEECourseWork/Mainproject/data/Vargas/filtered/trimfilter_out2.csv")
#saveRDS(out1, "/home/hanyun/CMEECourseWork/Mainproject/data/Vargas/filtered/trimfilter_out2.rds")
#BATCHES 2 and 3 LIMS 8717 and 8719 ====
# Sort ensures forward/reverse reads are in same order
#fnFs <- sort(list.files(path, pattern="_R1_"))
#fnRs <- sort(list.files(path, pattern="_R2_"))
#length(fnFs) #192 = 2 batches of 96, as expected
#length(fnRs) #192 = 2 batches of 96, as expected

# Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq
#sample.names <- sapply(strsplit(fnFs, "_"), `[`, 1)
# Specify the full path to the fnFs and fnRs
#fnFs <- file.path(path, fnFs)
#fnRs <- file.path(path, fnRs)

#filt_path <- file.path(path, "filtered") # Place filtered files in filtered/ subdirectory
#filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt_trimL.fastq.gz"))
#filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt_trimL.fastq.gz"))

#out2 <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, truncLen=c(230,150),
#                      maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE, trimLeft = 30,
#                      compress=TRUE, multithread=TRUE)
#out2

#write.csv(out2, "/Users/sarah/Dropbox/Lithuania2014/MiSeqData/allfastq/filtered/trimfilter_out2_batch2_3.csv")
#saveRDS(out2, "/Users/sarah/Dropbox/Lithuania2014/MiSeqData/allfastq/filtered/trimfilt_out2.rds")

#Look at trimfilter results ====

#see what proportion of reads I lost
out <- readRDS("/home/hanyun/CMEECourseWork/Mainproject/results/Main/trimfilter_out.rds")
result<-read.csv("/home/hanyun/CMEECourseWork/Mainproject/results/Main/trimfilter_out.csv")
#result2<-read.csv("/Users/sarah/Dropbox/Lithuania2014/MiSeqData/allfastq/filtered/trimfilter_out2_batch2_3.csv")

#result<-rbind(result1, result2) 

result$prop_retained<-result$reads.out/result$reads.in
summary(result$prop_retained) #Mean 90% reads retained after trim-filter. 
#Range (achieved by F:250, R:200)
#Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#0.1159  0.8096  0.8210  0.8203  0.8404  0.8952

write.csv(result, "/home/hanyun/CMEECourseWork/Mainproject/data/Vargas/filtered/trimfilter_out_result.csv")
mean(result$reads.in) #50902.22
mean(result$reads.out) #42431.52
mean(result$reads.out)/mean(result$reads.in) #83% reads retained after trim-filter
