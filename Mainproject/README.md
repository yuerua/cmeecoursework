#=== MY CMEE Coursework Directory - Main project ===  
  
Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk  
Date: Aug 2019  
  
##== Description ==  

Include the data, script and results for the CMEE Main project.  

##Files location tips
###[PDF document --- Mainproject/document/Zhang_Hanyun_CMEE_2019.pdf](https://bitbucket.org/yuerua/cmeecoursework/src/master/Mainproject/document/Zhang_Hanyun_CMEE_2019.pdf)  
###[R file --- Mainproject/code/main.R](https://bitbucket.org/yuerua/cmeecoursework/src/master/Mainproject/code/)  
###[data file --- Mainproject/data](https://bitbucket.org/yuerua/cmeecoursework/src/master/Mainproject/data/)  

Output files are saved to /results/Main
  
== List of files ==

├── code
│   ├── Cluster.R
│   ├── dada2                                                         #R scripts for the DADA2 pipeline
│   │   ├── 1.DADA2_decide_trim_parameters.R
│   │   ├── 2.DADA2_Trimfilter_allfastq_raw_trimLeft_local.R
│   │   ├── 3.DADA2_infer_seq_variants_run1_raw_trimLeft.R
│   │   ├── 4.DADA2_chimeras_taxonomy_assign.R
│   │   └── 5.Betadiversity.R
│   ├── fasta.R
│   └── main.R                                                        #R scripts for data analysis
├── data
│   ├── bird species                                                  #Sample metadata
│   │   ├── Bird_Community_TAG.csv
│   │   ├── Bird_Community_TAG_sorted.csv
│   │   ├── Bird_Community_TAG_sorted_remained.csv
│   │   ├── Bird_Community_TAG_sorted_remained_include_weak.csv
│   │   ├── bird_phylo_tree.nex
│   │   ├── bird-species-tree.nwk
│   │   └── sample.info.csv
│   ├── diet                                                           #Diet data
│   │   ├── binary_diet_by_species.csv
│   │   ├── Binary_diet.csv
│   │   └── info_all.xlsx
│   ├── location.png
│   └── taxonomy_datasets                                              #Trainning database
│       └── gg_13_8_train_set_97.fa.gz
├── document
│   ├── Cover.pdf
│   ├── Report.bib
│   ├── results
│   │   ├── ad_w.PNG
│   │   ├── ALL.png
│   │   ├── dw1.csv
│   │   ├── dw.csv
│   │   ├── fam_df.csv
│   │   ├── family_by_sample.pdf
│   │   ├── Figure2.pdf
│   │   ├── Figure3.pdf
│   │   ├── Figure3.PNG
│   │   ├── FS1.pdf
│   │   ├── hm.PNG
│   │   ├── imperial.png
│   │   ├── otu_jxk895_include.csv
│   │   ├── otu_occur_species_unique.csv
│   │   ├── otu_prev.csv
│   │   ├── phy_df.csv
│   │   ├── phylum_by_sample.pdf
│   │   └── sample.info_species.csv
│   ├── Thesis.tex                                                      #Latex file
│   └── Zhang_Hanyun_CMEE_2019.pdf                                      #Thesis pdf
├── README.md
└── results
    └── Main

10 directories, 43 files

