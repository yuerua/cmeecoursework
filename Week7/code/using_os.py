#!/usr/bin/env python3

""" Use the subprocess.os module to get a list of files 
and directories with defined pattern in the home directory """ 

__appname__ = 'using_os.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess
import re

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home):
    for filename in files:
         if re.match(r"C", filename): # re.match capture any files and directory with names starts with C
            FilesDirsStartingWithC.append(filename)
    for directory in subdir:
        if re.match(r"C", directory):
            FilesDirsStartingWithC.append(directory)
print(FilesDirsStartingWithC)
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:
FilesDirsStartingWithCc = []
for (dir, subdir, files) in subprocess.os.walk(home):
    for filename in files:
         if re.match(r"[Cc]", filename):
            FilesDirsStartingWithCc.append(filename)
    for directory in subdir:
        if re.match(r"[Cc]", directory):
            FilesDirsStartingWithCc.append(directory)
print(FilesDirsStartingWithCc)

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:
DirsStartingWithCc = []
for (dir, subdir, files) in subprocess.os.walk(home):
    for directory in subdir:
        if re.match(r"[Cc]", directory):
            DirsStartingWithCc.append(directory)
print(DirsStartingWithCc)





