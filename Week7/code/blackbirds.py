#!/usr/bin/env python3

""" Extract information using regular expression """

__appname__ = 'blackbirds.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import re
import pandas as pd #Use panda to create a dataframe for the results

# Read the file
f = open('../data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
#text = text.decode('ascii', 'ignore') #will not work in python 3

# Now extend this script so that it captures the Kingdom, 
# Phylum and Species name for each species and prints it out to screen neatly.

# Hint: you may want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 
# Your solution may involve multiple regular expression calls (easier!), or a single one (harder!)
Kingdom = re.findall(r'Kingdom\s+(\w+)', text)
Phylum = re.findall(r'Phylum\s+(\w+)', text)
Species = re.findall(r'Species\s+([A-Z]\w+\s+\w+)', text)

output = pd.DataFrame()
output["Species name"] = Species
output["Phylum"] = Phylum
output["Kingdom"] = Kingdom

print(output)

