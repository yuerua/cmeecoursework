#!/usr/bin/env python3

"""Calculate and plot Lotka-Volterra model"""

__appname__ = 'L1.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import scipy as sc

# Function
def dCR_dt(pops, t=0): # Time defaults to 0
    """ Calculate the growth rate of consumer and resource
    population based on Lotka-Volterra model at given time step""" 
    R = pops[0]
    C = pops[1]
    dRdt = r * R - a * R * C 
    dCdt = -z * C + e * a * R * C
    return sc.array([dRdt, dCdt])

# Assign parameters in LV model
r = 1.    #Growth rate of the resource population
a = 0.1   #Search rate" for the resource multiplied by its attack success probability
z = 1.5   #Consumer mortality rate 
e = 0.75  #Consumer's efficiency of production

# Define time vector (stimulated continuous time series) 
t = sc.linspace(0, 15,  1000) # using 1000 sub-divisions of time

# Set initial population
R0 = 10 # resources per unit area
C0 = 5  # consumers per unit area
RC0 = sc.array([R0, C0]) 

# Integrate.odeint takes arguments and apply the function for t times, and
# returns results as an array. Infodict is a dictionary containing info of integrate function 
import scipy.integrate as integrate
pops, infodict = integrate.odeint(dCR_dt, RC0, t, full_output=True)
print (" The final resource density is %s, consumer density is %s"%(pops[len(t)-1, 0], pops[len(t)-1, 1]))

# Plot population density against time using matplotlib.pylab
import matplotlib.pylab as p
f1 = p.figure()
p.plot(t, pops[:,0], 'g-', label='Resource density') 
p.plot(t, pops[:,1]  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='upper left')
p.xlabel('Time')
p.ylabel('Population density')
p.title('Consumer-Resource population dynamics')


# Plot consumer density against resource density
f2 = p.figure()
p.plot(pops[:,0], pops[:,1], 'r-')
p.grid()
p.xlabel('Resource density')
p.ylabel('Consumer density')
p.title('Consumer-Resource population dynamics')

# Save both figures into a single pdf
import matplotlib.backends.backend_pdf
pdf = matplotlib.backends.backend_pdf.PdfPages('../results/LV_model1.pdf')
pdf.savefig(f1)
pdf.savefig(f2)
pdf.close()

