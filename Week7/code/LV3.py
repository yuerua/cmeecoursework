#!/usr/bin/env python3

"""Calculate and plot discrete-time version of the Lotka-Volterra model"""

__appname__ = 'L3.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports
import sys
import scipy as sc

# Function
def dCR_dt(pops, t=0): # Time defaults to 0
    """ Calculate the population size of consumer and resource
    based on Lotka-Volterra model at given time step""" 
    # Initiate an array to store the results
    N = sc.zeros((t,2), dtype = float)
    N[0,0] = pops[0]
    N[0,1] = pops[1]
    for i in range(t-1):
        N[i+1,0] = N[i,0]*(1 + r * (1 - (N[i,0]/K)) - a * N[i, 1])
        N[i+1,1] = N[i,1]*(1 - z + e * a * N[i,0])
    return N

# Parameters
if len(sys.argv) == 6:
    r = float(sys.argv[1])  #Growth rate of the resource population
    a = float(sys.argv[2])  #Search rate" for the resource multiplied by its attack success probability
    z = float(sys.argv[3])  #Consumer mortality rate 
    e = float(sys.argv[4])  #Consumer's efficiency of production
    K = float(sys.argv[5])  #Carrying capacity
else:
    r = 1.2   
    a = 0.1
    z = 0.5
    e = 0.75
    K = 20
        
# Define time steps
t = 100

# Set initial population
R0 = 10 # resources per unit area
C0 = 5  # consumers per unit area
RC0 = sc.array([R0, C0])

pops = dCR_dt(RC0, t)

print("After %s years, the resource density is %s, the consumer density is %s" %(t-1, pops[t-1,0], pops[t-1,1]))

# Plot population density against time steps using matplotlib.pylab
import matplotlib.pylab as p

f1 = p.figure()
p.plot(range(t), pops[:,0], 'g-', label='Resource density') 
p.plot(range(t), pops[:,1], 'b-', label='Consumer density')
p.grid()
p.legend(loc='upper left')
p.xlabel('Time')
p.ylabel('Population density')
p.title('Consumer-Resource population dynamics')
# Annotate figure with parameter values
p.figtext(0.78, 0.12, 'r = %s\na = %s\nz = %s\ne = %s\nK = %s'%(r, a, z, e, K), size = 12)

# Plot consumer density against resource density
f2 = p.figure()
p.plot(pops[:,0], pops[:,1], 'r-')
p.grid()
p.xlabel('Resource density')
p.ylabel('Consumer density')
p.title('Consumer-Resource population dynamics')
# Annotate figure with parameters
p.figtext(0.78, 0.12, 'r = %s\na = %s\nz = %s\ne = %s\nK = %s'%(r, a, z, e, K), size = 12)

# Save both figures into a single pdf
import matplotlib.backends.backend_pdf
pdf = matplotlib.backends.backend_pdf.PdfPages('../results/LV_model3.pdf')
pdf.savefig(f1)
pdf.savefig(f2)
pdf.close()
