#!/usr/bin/env python3

""" A python version of Nets.R that plots 
the QMEE CDT collaboration network """

__appname__ = 'Nets.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1' 

# Imports
import networkx as nx
import pandas as pd
import scipy as sc
import matplotlib.pyplot as p

# Data
links = pd.read_csv('../data/QMEE_Net_Mat_edges.csv')
nodes = pd.read_csv('../data/QMEE_Net_Mat_nodes.csv')

## Extract institution ids from nodes
id = sc.array(nodes['id'])

## Generate connections 
idx = sc.where(links.as_matrix())# Returns two arrays containing the row and column index of PhD students
# Initiate two lists to store the institution ids corresponding to index
x = []
y = []
for i in idx[0]:
    x.append(id[i])
for i in idx[1]:
    y.append(id[i])

# Returns tuples of institution ID pairs
edge = zip(x, y)

# Set edge width based on weight(PhD students)
width = 1 + links.as_matrix()[idx]/10
# Set node sizes
size = nodes['Pis']*50
# Set node colors(there must be a better way to allocate...)
colrs = ["g", "r", "b"]
nodes['color'] = ""
nodes['color'].loc[nodes['Type'] == 'University'] = colrs[2]
nodes['color'].loc[nodes['Type'] == 'Hosting Partner'] = colrs[0]
nodes['color'].loc[nodes['Type'] == 'Non-Hosting Partners'] = colrs[1]
# Set layout
pos = nx.spring_layout(id)

# Generate a graph object
net = nx.Graph()
# Add nodes and links
net.add_nodes_from(id)
net.add_edges_from(edge)

#Plots the graph
f = p.figure()
nx.draw_networkx(net, pos = pos, node_size = size, node_color = nodes['color'], 
edge_color = "grey", width = width)
f.savefig('../results/Nets.svg')




