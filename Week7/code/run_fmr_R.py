#!/usr/bin/env python3

""" Run a R script with subprocess module and test if it runs successfully """ 

__appname__ = 'run_fmr_R.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess

p = subprocess.Popen("Rscript fmr.R", stdout=subprocess.PIPE,\
stderr=subprocess.PIPE, shell=True)

stdout, stderr = p.communicate()

if stdout:
    print(stdout.decode())
    print("\nSucceed!\n")
else:
    print(stderr.decode())
    print("\nError found!\n")

