#!/usr/bin/env python3

""" Run LV model scripts and profile them with run -p in command line"""

__appname__ = 'run_LV.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess

subprocess.Popen("python3 LV1.py", shell=True)
subprocess.Popen("python3 LV2.py 1.0 0.1 0.5 0.75 1000", shell=True).wait()
subprocess.Popen("python3 LV3.py 1.2 0.1 0.5 0.75 20", shell=True).wait()
subprocess.Popen("python3 LV4.py 1.2 0.1 0.5 0.75 15", shell=True).wait()


