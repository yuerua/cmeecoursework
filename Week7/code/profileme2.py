#!/usr/bin/env python3

"""Boilerplate for profiling, with speed improved by using list comprehension
   and explicit string concatenation"""

__appname__ = 'profileme2.py'
__author__ = 'Hanyun Zhang(hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

def my_squares(iters):
    """ Compute the squre of inputs and output to a list using list comprehension"""
    out = [i ** 2 for i in range(iters)]
    return out

def my_join(iters, string):
    """ Join inputs with explicit string concatenation """
    out = ''
    for i in range(iters):
        out += ", " + string
    return out

def run_my_funcs(x,y):
    """ Apply each function respectively with elements of input """
    print(x,y)
    my_squares(x)
    my_join(x,y)
    return 0

run_my_funcs(10000000,"My string")