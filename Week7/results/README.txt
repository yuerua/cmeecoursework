=== MY CMEE Coursework Directory - Week 7 ===

Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
Date: Nov 2018

== Description ==

Include the script, data and sandbox files for the 7th week practicals.

# Input files are in /data #
# Output files are saved to /results #

== List of files ==         == Description == 

├── code
│   ├── blackbirds.py       # Extract information using regular expression
│   ├── DrawFW.py           # Plot a food web network with synthetic data
│   ├── fmr.R               # R script with csv input and pdf output 
│   ├── LV1.py              # Calculate and plot Lotka-Volterra model
│   ├── LV2.py              # Calculate and plot Lotka-Volterra model with parameters taken from the command line
│   ├── LV3.py              # Calculate and plot discrete-time version of the Lotka-Volterra model
│   ├── LV4.py              # Calculate and plot discrete-time version of LV model with simulation with a random gaussian fluctuation in population growth rate"
│   ├── Nets.py             # Python version of Nets.R
│   ├── Nets.R              # Plot the QMEE CDT collaboration network 
│   ├── profileme2.py       # Boilerplate for profiling, with speed improved by using list comprehension and explicit string concatenation
│   ├── profileme.py        # Boilerplate for profiling
│   ├── run_fmr_R.py        # Run a R script with subprocess module and test if it runs successfully
│   ├── run_LV.py           # Run LV model scripts and profile them
│   ├── TestR.py            # Run a R script with python and returns output or error
│   ├── timeitme.py         # A test for profiling with timeit
│   └── using_os.py         # Use subprocess.os module to get a list of files and directories with defined pattern in the home directory
├── data
│   ├── blackbirds.txt
│   └── NagyEtAl1999.csv
│   ├── QMEE_Net_Mat_edges.csv
│   └── QMEE_Net_Mat_nodes.csv
├── README.txt
├── results
└── sandbox


4 directories, 24 files
