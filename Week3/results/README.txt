=== MY CMEE Coursework Directory - Week 3 ===

Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
Date: Oct 2018

== Description ==

Include the script, data and sandbox files for the 3rd week practicals.

# Input files are in /data #
# Output files are saved to /results #

== List of files ==               == Description == 

├── code
│   ├── apply2.R                  # Applying the same function to rows/colums of a matrix
│   ├── apply.R                   # Applying the same function to rows/colums of a matrix
│   ├── basic_io.R                # Example of data input and output in R
│   ├── boilerplate.R             # A boilerplate R script
│   ├── break.R                   # Example of breaking out of loops
│   ├── browse.R                  # Debugging with browser
│   ├── control.R                 # Some code exemplifying control flow constructs in R   
│   ├── DataWrang.R               # Examples of data wrangling using reshape2
│   ├── DataWrangTidy.R           # Examples of data wrangling using dplyr and tidyr
│   ├── get_TreeHeight.py         # Calculates heights of trees given distance of each tree from its base and angle to its top, using  the trigonometric formula
│   ├── get_TreeHeight.R          # Calculate tree heights and output to a file with file name defined by the input file name
│   ├── Girko.R                   # Plot the Girko's law simulation
│   ├── LaTeX 
│   │   ├── TAutoCorr.pdf 
│   │   └── TAutoCorr.Rnw         # Generate pdf with knitr
│   ├── maps.R                    # Draw a world map and plot all the locations from the data set
│   ├── MyBars.R                  # Examples of plot annotation
│   ├── next.R                    # Skip to next iteration of a loop with next
│   ├── plotLin.R                 # Plot and annotate a linear regression model
│   ├── PP_Lattice.R              # Draws and saves three lattice graphs by feeding interaction type
│   ├── PP_Regress_loc.R          # Draw a regression plot with linear model with the data grouped by type of feeding interaction, predator lifestage and location
│   ├── PP_Regress.R              # Draw a regression plot with linear model with the data grouped by type of feeding interaction and predator lifestage
│   ├── run_get_TreeHeight.sh     # Run get_TreeHeight.R with trees.csv as an example 
│   ├── sample.R                  # Examples for vectorization
│   ├── TAutoCorr.R               # Investigate the correlation between the temperature of successive years
│   ├── TreeHeight.R              # Calculate the tree heights
│   ├── try.R                     # Run a simulation that involves sampling from a population with try
│   ├── Vectorize1.py             # A python version of Vectorize1.R, sums all elements in a matrix using loop or vectorization
│   ├── Vectorize1.R              # Different ways to run the stochastic Ricker model
│   ├── Vectorize2.py             # A python version of Vectorize2.R, uses different ways to run the stochastic Ricker model
│   ├── Vectorize2.R              # Different ways to run the stochastic Ricker model
│   └── Vectorize_Compare.sh      # Run four Vectorize scripts and compare their computational speed
├── data
│   ├── EcolArchives-E089-51-D1.csv
│   ├── GPDDFiltered.RData
│   ├── KeyWestAnnualMeanTemperature.RData
│   ├── PoundHillData.csv
│   ├── PoundHillMetaData.csv
│   ├── Results.txt
│   └── trees.csv
├── README.txt
├── results
└── sandbox
