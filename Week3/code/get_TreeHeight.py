#!/usr/bin/env python3

""" A function calculates heights of trees given distance of each tree 
from its base and angle to its top, using  the trigonometric formula"""

__appname__ = 'get_TreeHeight.py'
__author__ = 'Hanyun Zhang (hanyun.zhang18@imperial.ac.uk)'
__version__ = '0.0.1'

## Import
import csv
import sys
import numpy as np # For math calculation
import pandas

## Data
f = open(sys.argv[1],"r")

# Pandas allows the csv file to be read as a dataframe
Data = pandas.read_csv(f)

## Function
def TreeHeight(degrees, distance):
    """ Calculate treeheights based on degrees and distance """
    radians = np.deg2rad(degrees)
    height = distance * np.tan(radians)
    return (height)

# Takes the degrees and distance from corresponding columns, calculate treeheights
# and saves to a new column
Data["Tree.Height.m"] = TreeHeight(Data["Angle.degrees"],Data["Distance.m"])

## Output 
# Define the name of the output file based on filename of the argument
# Copy the comment line argument
path = sys.argv[1]
# Extract the filename from input path
filename = path.split("/")[-1].split(".")[0]
# Set the output file
output__path = "../results/" + filename + "_treeheights.csv"
# Save to the defined output file
Data.to_csv(output__path)

f.close()







