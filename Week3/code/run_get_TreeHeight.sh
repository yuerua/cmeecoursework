#!/bin/bash
# run_get_TreeHeight.sh 
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Desc: run get_TreeHeight.R with trees.csv as an example 
# Arguments: 1-> ../data/trees.csv
# Date: Oct 2018

Rscript get_TreeHeight.R ../data/trees.csv
