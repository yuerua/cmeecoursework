# Vectorize1.R
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
# Desc: sums all elements in a matrix using loop or vectorization
# Arguments: none
# Date: Oct 2018

M <- matrix(runif(1000000),1000,1000)

SumAllElements <- function(M){
  Dimensions <- dim(M)
  Tot <- 0
  for (i in 1:Dimensions[1]){
    for (j in 1:Dimensions[2]){
      Tot <- Tot + M[i,j]
    }
  }
  return (Tot)
}

print(paste("Sum of all elements in the matrix is", sum(M)))
print("Sum with looping takes:")
print(system.time(SumAllElements(M)))
print("Sum with built in function takes:")
print(system.time(sum(M))) # Inbuilt function sum() uses vectorization to avoid looping