#!/bin/bash
# Script: Vectorize_Compare.sh
# Author: Hanyun Zhang hanyun.zhang18@imperial.ac.uk
#Desc: Run four Vectorize scripts and compare their computational speed
#Arguments: none
# Date: Oct 2018
echo -e "\n Computing time for Vectorize1.R \n"
Rscript Vectorize1.R
echo -e "\n Computing time for Vectorize1.py \n"
python3 Vectorize1.py
echo -e "\n Computing time for Vectorize2.R \n"
Rscript Vectorize2.R
echo -e "\n Computing time for Vectorize2.py \n"
python3 Vectorize2.py
